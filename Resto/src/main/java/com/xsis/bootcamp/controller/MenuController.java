package com.xsis.bootcamp.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xsis.bootcamp.model.MenuModel;
import com.xsis.bootcamp.service.MenuService;

@Controller
public class MenuController {
	
private Log log = LogFactory.getLog(getClass());
	
	@Autowired
	private MenuService service;
	
	@RequestMapping(value="/menu")
	public String index(Model model) {
		return "menu";
	}
	
	@RequestMapping(value="/menu/list")
	public String list(Model model){
		// membuat object list dari class menu model
		List<MenuModel> items = null;
		
		try {
			// object items diisi data dari method get
			items = this.service.get();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		// datanya kita kirim ke view, 
		// kita buat variable list kemudian diisi dengan object items
		model.addAttribute("list", items);
		
		return "menu/list";
	}
	
	@RequestMapping(value="/menu/detail")
	public String detail(Model model){
		// membuat object list dari class menu model
		List<MenuModel> items = null;
		
		try {
			// object items diisi data dari method get
			items = this.service.get();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		// datanya kita kirim ke view, 
		// kita buat variable list kemudian diisi dengan object items
		model.addAttribute("list", items);
		
		return "menu/detail";
	}

	@RequestMapping(value="/menu/add")
	public String add(){
		return "menu/add";
	}
	
	@RequestMapping(value="/menu/save")
	public String save(Model model, @ModelAttribute MenuModel item, HttpServletRequest request){
		String proses = request.getParameter("proses");
		String result ="";
		// proses input ke database
		try {
			if(proses.equals("insert")){
				this.service.insert(item);
			}else if(proses.equals("update")){
				this.service.update(item);
			}else if(proses.equals("delete")){
				this.service.delete(item);
			}
			
			result="berhasil";
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			result ="gagal";
		}
		model.addAttribute("result",result);
		
		return "menu/save";
	}
	
	@RequestMapping(value="/menu/edit")
	public String edit(Model model, HttpServletRequest request){
		// menangkap paremeter yang dikirim dari view
		int id = Integer.parseInt(request.getParameter("id_mkn"));
		
		// siapkan object menu model
		MenuModel item = null;
		// request ke database
		try {
			item = this.service.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		// datanya kita kirim ke view, 
		// kita buat variable item kemudian diisi dengan object item
		model.addAttribute("item", item);
		return "menu/edit";
	}
	
	@RequestMapping(value="/menu/delete")
	public String delete(Model model, HttpServletRequest request){
		// menangkap paremeter yang dikirim dari view
		int id = Integer.parseInt(request.getParameter("id_mkn"));
		
		// siapkan object menu model
		MenuModel item = null;
		// request ke database
		try {
			item = this.service.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("item", item);
		return "menu/delete";
	}
}
