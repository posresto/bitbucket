package com.xsis.bootcamp.controller;


import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.xsis.bootcamp.model.SupplierModel;
import com.xsis.bootcamp.model.BahanModel;
import com.xsis.bootcamp.model.DetailPembelianModel;
import com.xsis.bootcamp.model.PembelianModel;
import com.xsis.bootcamp.service.SupplierService;
import com.xsis.bootcamp.service.BahanService;
import com.xsis.bootcamp.service.PembelianDtlService;
import com.xsis.bootcamp.service.PembelianService;;


@Controller
public class PembelianController {
	private Log log = LogFactory.getLog(getClass());


	@Autowired
	private BahanService bahanService;
	
	@Autowired
	private PembelianService beliService;
	
	@Autowired
	private PembelianDtlService dtlService;
	
	@Autowired
	private SupplierService supService;

	List<DetailPembelianModel> dpmList = null;
	
	@RequestMapping(value="/listbeli")
	public String LiastReservasi(Model model){
		
		return "listbeli";
	}
	
	// list pembelian
	@RequestMapping(value="/pembelian/list")
	public String list(Model model) {
		
		List<PembelianModel> items = null;
		
		try {
			items = this.beliService.get();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		model.addAttribute("listBeli", items);
		
		return "pembelian/list";
	}
	
	@RequestMapping(value="/pembelian/detail")
	public String detail(Model model, HttpServletRequest request) {
		
		String id = request.getParameter("id");
		PembelianModel item = null;
		
		try {
			item = this.beliService.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		model.addAttribute("item", item);
		
		return "pembelian/detail";
	}
	
	
	// HEADER //
	@RequestMapping(value="/pembelian")
	public String index(Model model,HttpServletRequest request) {
		List<SupplierModel> supplier = null;
		dpmList = new ArrayList<DetailPembelianModel>();		
		
		try {
			supplier = this.supService.get();
		} catch (Exception e) {
			
		}
		
		model.addAttribute("listSupplier", supplier);
		
		return "pembelian";
	}
	
	
	// DETAIL //
	@RequestMapping(value="/pembelian/pilih_bahan")
	public String pilih_detail(Model model, HttpServletRequest request,@RequestParam(value="itemPilih") String[] itemIdPilih) throws Exception{
		
		for (int i = 0; i < itemIdPilih.length; i++) {
			itemIdPilih[i] =itemIdPilih[i].replace("[", "").replace("]", "").replace("\"", "");
			
			BahanModel im = new BahanModel();
			im = this.bahanService.getById(Integer.valueOf(itemIdPilih[i]));
			
			DetailPembelianModel jdm = new DetailPembelianModel();
			jdm.setId_bhn(im.getId());
			// yang dirubah
			jdm.setBahan(im);
			
			
			dpmList.add(jdm);
		}
		 model.addAttribute("dpmList", dpmList);
		 model.addAttribute("success", "success");
		
		
		return "pembelian";
	}
	
	//Tambah Menu
	@RequestMapping(value = "/pembelian/add")
	public String add(Model model) {
		List<BahanModel> bahan = null;
		String idExcludeList = "";
		try {
			// object items diisi data dari method get
			idExcludeList = this.getIdExcludeList(dpmList);
			bahan = this.bahanService.getByIdNotIn(idExcludeList);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}

		// datanya kita kirim ke view,
		// kita buat variable list kemudian diisi dengan object items
		model.addAttribute("listBahan", bahan);
		return "pembelian/add";
	}
	
	
	// batal pesan
		@RequestMapping(value = "/pembelian/batal_beli")
		public String batal_pesan(Model model, HttpServletRequest request) {

			int id = Integer.valueOf(request.getParameter("idx"));
			if (dpmList.size() > 0) {
				for (int i = 0; i < dpmList.size(); i++) {
					int idBahan = dpmList.get(i).getId_bhn();
					if (id == idBahan) {
						dpmList.remove(i);
					}
				}
			}

			model.addAttribute("dpmList", dpmList);
			model.addAttribute("success", "success");
			return "pembelian";
		}

		
		// save transaksi
		@RequestMapping(value="/pembelian/simpan")
		public String cetak(Model model, HttpServletRequest request) throws Exception {
			
			String result ="";
			//Get data dari form header
			String id_beli = request.getParameter("id");
			String id_sup = request.getParameter("id_sup");
			String ket = request.getParameter("ket");
			String total_beli = request.getParameter("total_beli");
			String tipe_bayar = request.getParameter("tipe_bayar");
			String bayar = request.getParameter("bayar");
			String kembalian = request.getParameter("kembalian");
			
			String tgl_pembelian = request.getParameter("tgl");
			/*SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			Date tglBeli = formatter.parse(tgl_pembelianStr);*/
			
			//Set data dari form header ke model header
			PembelianModel bhm = new PembelianModel();
				bhm.setId(id_beli);
				bhm.setId_sup(Integer.valueOf(id_sup));
					SupplierModel supplier = new SupplierModel();
					supplier = this.supService.getById(Integer.valueOf(id_sup));			
				bhm.setSup(supplier);
				bhm.setId_sup(Integer.valueOf(id_sup));
				bhm.setTotal(Integer.valueOf(total_beli));
				bhm.setTp_bayar(tipe_bayar);
				bhm.setBayar(Integer.valueOf(bayar));
				bhm.setKembalian(Integer.valueOf(kembalian));
				bhm.setTgl(tgl_pembelian);
				bhm.setKet(ket);
			
			
				
			try {
				//simpan pembelian header
				this.beliService.insert(bhm);
				String headerID = bhm.getId();
				
				String jumlahDetail = request.getParameter("jumlahDetail");
				int jumlahDetailInteger = Integer.valueOf(jumlahDetail);
				
				
				for (int i = 0; i < jumlahDetailInteger; i++) {
					//Set per detil satu satu
					String idBahan = request.getParameter("idBahan_"+i);
					String satuan = request.getParameter("satuan_"+i);
					String hargabeli = request.getParameter("hargabeli_"+i);
					String qtyDetail = request.getParameter("qty_"+i);
					String hasilDetail = request.getParameter("hasil_"+i);
					String namaBhn = request.getParameter("namaBhn_"+i);
					
					BahanModel item = new BahanModel();
					item = this.bahanService.getById(Integer.valueOf(idBahan));
					
					
					DetailPembelianModel bdm = new DetailPembelianModel();
						bdm.setId_bhn(Integer.valueOf(idBahan));
						// rubah sementara
						bdm.setBahan(item);
						bdm.setSatuan(satuan);
						bdm.setId_bli(headerID);
						bdm.setNama_bhn(namaBhn);
						bdm.setHarga_beli(Integer.valueOf(hargabeli));
						bdm.setQty(Integer.valueOf(qtyDetail));
						bdm.setSubtotal_beli(Integer.valueOf(hasilDetail));
					
					//Simpan per detil satu satu
					this.dtlService.insert(bdm);
					// bagian menambah bahan di corenya
					int qty_now = item.getStok();
					int qty_input = Integer.valueOf(qtyDetail);
					int qty_baru = qty_now + qty_input;
					item.setStok(qty_baru);
					this.bahanService.update(item);
					
				}
				
				result="berhasil";
				
			} catch (Exception e) {
				// TODO: handle exception
				log.error(e.getMessage(), e);
				result ="gagal";
				
			}
			
			model.addAttribute("result",result);
			
			return "pembelian";
		}
		
		// list ex
		public String getIdExcludeList(List<DetailPembelianModel> dpmList) {
			String idExcludeList = "";
			String comma = "";
			if (dpmList.size() > 0) {
				for (int i = 0; i < dpmList.size(); i++) {
					if (i > 0) {
						comma = ",";
					} else {
						comma = " ";
					}

					int id = dpmList.get(i).getId_bhn();
					idExcludeList = idExcludeList + comma + id;
				}
			}

			return idExcludeList;
		}

	}