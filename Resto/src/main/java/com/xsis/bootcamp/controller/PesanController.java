package com.xsis.bootcamp.controller;


import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.xsis.bootcamp.model.MejaModel;
import com.xsis.bootcamp.model.MenuModel;
import com.xsis.bootcamp.model.PesanDtlModel;
import com.xsis.bootcamp.service.MejaService;
import com.xsis.bootcamp.service.MenuService;


@Controller
public class PesanController {
	private Log log = LogFactory.getLog(getClass());


	@Autowired
	private MenuService menuService;
	
	@Autowired
	private MejaService mejaService;

	List<PesanDtlModel> pdmList = null;
	
	
	// HEADER //
	@RequestMapping(value="/pesan")
	public String index(Model model,HttpServletRequest request) {
		List<MejaModel> meja = null;
		pdmList = new ArrayList<PesanDtlModel>();		
		
		try {
			meja = this.mejaService.get();
		} catch (Exception e) {
			
		}
		
		model.addAttribute("listMeja", meja);
		
		return "pesan";
	}
	
	
	// DETAIL //
	@RequestMapping(value="/pesan/pilih_menu")
	public String pilih_detail(Model model, HttpServletRequest request,@RequestParam(value="itemPilih") String[] itemIdPilih) throws Exception{
		
		for (int i = 0; i < itemIdPilih.length; i++) {
			itemIdPilih[i] =itemIdPilih[i].replace("[", "").replace("]", "").replace("\"", "");
			
			MenuModel im = new MenuModel();
			im = this.menuService.getById(Integer.valueOf(itemIdPilih[i]));
			
			PesanDtlModel jdm = new PesanDtlModel();
			jdm.setId_mn(im.getId_mkn());
			jdm.setMenu(im);
			
			
			pdmList.add(jdm);
		}
		 model.addAttribute("pdmList", pdmList);
		 model.addAttribute("success", "success");
		
		
		return "penjualan_header";
	}
	
	//Tambah Menu
	@RequestMapping(value = "/pesan/add")
	public String add(Model model) {
		List<MenuModel> menus = null;
		String idExcludeList = "";
		try {
			// object items diisi data dari method get
			idExcludeList = this.getIdExcludeList(pdmList);
			menus = this.menuService.getByIdNotIn(idExcludeList);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}

		// datanya kita kirim ke view,
		// kita buat variable list kemudian diisi dengan object items
		model.addAttribute("list", menus);
		return "pesan/add";
	}
	
	
	// batal pesan
		@RequestMapping(value = "/pesan/batal_pesan")
		public String batal_pesan(Model model, HttpServletRequest request) {

			int id = Integer.valueOf(request.getParameter("idx"));
			if (pdmList.size() > 0) {
				for (int i = 0; i < pdmList.size(); i++) {
					int idMenu = pdmList.get(i).getId_mn();
					if (id == idMenu) {
						pdmList.remove(i);
					}
				}
			}

			model.addAttribute("pdmList", pdmList);
			model.addAttribute("success", "success");
			return "pesan";
		}

		// list ex
		public String getIdExcludeList(List<PesanDtlModel> pdmList) {
			String idExcludeList = "";
			String comma = "";
			if (pdmList.size() > 0) {
				for (int i = 0; i < pdmList.size(); i++) {
					if (i > 0) {
						comma = ",";
					} else {
						comma = " ";
					}

					int id = pdmList.get(i).getId_mn();
					idExcludeList = idExcludeList + comma + id;
				}
			}

			return idExcludeList;
		}

	}
