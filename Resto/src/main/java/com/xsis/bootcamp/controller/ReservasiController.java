package com.xsis.bootcamp.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.xsis.bootcamp.model.DetailReservasiModel;
import com.xsis.bootcamp.model.MejaModel;
import com.xsis.bootcamp.model.ReservasiModel;
import com.xsis.bootcamp.service.MejaService;
import com.xsis.bootcamp.service.ReservasiDtlService;
import com.xsis.bootcamp.service.ReservasiService;

@Controller
public class ReservasiController {

	private Log log = LogFactory.getLog(getClass());

	@Autowired
	private MejaService service;
	
	@Autowired
	private ReservasiService resvService;
	
	@Autowired
	private ReservasiDtlService resvDtlService;

	List<DetailReservasiModel> rsvList = null;

	@RequestMapping(value = "/reservasi")
	public String index(Model model) {
		rsvList = new ArrayList<DetailReservasiModel>();
		return "reservasi";
	}

	@RequestMapping(value="/listrsv")
	public String LiastReservasi(Model model){
		
		return "listrsv";
	}
	
	// list reservasi
	@RequestMapping(value="/reservasi/list")
	public String list(Model model){
		// membuat object list dari class meja model
		List<ReservasiModel> items = null;
		
		try {
			// object items diisi data dari method get
			items = this.resvService.get();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		// datanya kita kirim ke view, 
		// kita buat variable list kemudian diisi dengan object items
		model.addAttribute("listReservasi", items);
		
		return "reservasi/list";
	}
	
	// detail reservasi
	@RequestMapping(value="/reservasi/detail")
	public String detail(Model model, HttpServletRequest request){
		// menangkap paremeter yang dikirim dari view
		int id = Integer.parseInt(request.getParameter("id"));
		
		// siapkan object Item model
		ReservasiModel item = null;
		// request ke database
		try {
			item = this.resvService.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		// datanya kita kirim ke view, 
		// kita buat variable item kemudian diisi dengan object item
		model.addAttribute("item", item);
		return "reservasi/detail";
	}
	
	// pilih meja
	@RequestMapping(value = "/reservasi/pilih_meja")
	public String pilih_detail(Model model, HttpServletRequest request,
			@RequestParam(value = "itemPilih") String[] itemIdPilih) throws Exception {

		for (int i = 0; i < itemIdPilih.length; i++) {
			itemIdPilih[i] = itemIdPilih[i].replace("[", "").replace("]", "").replace("\"", "");

			MejaModel im = new MejaModel();
			im = this.service.getById(Integer.valueOf(itemIdPilih[i]));

			DetailReservasiModel jdm = new DetailReservasiModel();
			jdm.setId_mj(im.getId());
			jdm.setMeja(im);

			rsvList.add(jdm);
		}
		model.addAttribute("rsvList", rsvList);
		model.addAttribute("success", "success");

		return "reservasi";
	}
	
	// tambah meja
	@RequestMapping(value = "/reservasi/add")
	public String add(Model model) {

		List<MejaModel> listMeja = null;
		String idExcludeList = "";
		try {
			idExcludeList = this.getIdExcludeList(rsvList);
			listMeja = this.service.getByIdNotIn(idExcludeList);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("listMeja", listMeja);
		return "reservasi/add";
	}

	// batal meja
	@RequestMapping(value = "/reservasi/batal_meja")
	public String batal_meja(Model model, HttpServletRequest request) {

		int id = Integer.valueOf(request.getParameter("idx"));
		if (rsvList.size() > 0) {
			for (int i = 0; i < rsvList.size(); i++) {
				int idMeja = rsvList.get(i).getId_mj();
				if (id == idMeja) {
					rsvList.remove(i);
				}
			}
		}

		model.addAttribute("rsvList", rsvList);
		model.addAttribute("success", "success");
		return "reservasi";
	}
	
	@RequestMapping(value="/reservasi/simpan")
	public String cetak(Model model, HttpServletRequest request) throws Exception {
		
		
		//Get data dari form header
		String nama_rsv = request.getParameter("nama");
		String hp_rsv = request.getParameter("hp");
		String jam_rsv = request.getParameter("jam");
		String ket = request.getParameter("ket");
		
		String tgl_rsv = request.getParameter("tgl");
		/*SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date tglBeli = formatter.parse(tgl_pembelianStr);*/
		
		//Set data dari form header ke model header
		ReservasiModel bhm = new ReservasiModel();			
			bhm.setNama(nama_rsv);
			bhm.setHp(hp_rsv);
			bhm.setJam(jam_rsv);
			bhm.setKet(ket);
			bhm.setTgl(tgl_rsv);
		
		String result ="";
			
		try {
			//simpan pembelian header
			this.resvService.insert(bhm);
			Integer headerID = bhm.getId();
			
			String jumlahDetail = request.getParameter("jumlahDetail");
			int jumlahDetailInteger = Integer.valueOf(jumlahDetail);
			
			
			for (int i = 0; i < jumlahDetailInteger; i++) {
				//Set per detil satu satu
				String idMejaDtl = request.getParameter("idMeja_"+i);
				String nomorDtl = request.getParameter("nomor_"+i);
				String tipeDtl = request.getParameter("tipe_"+i);
				String ketDtl = request.getParameter("ket_"+i);
				
				MejaModel item = new MejaModel();
				item = this.service.getById(Integer.valueOf(idMejaDtl));
				
				
				DetailReservasiModel bdm = new DetailReservasiModel();
					bdm.setId_mj(Integer.valueOf(idMejaDtl));
					// rubah sementara
					bdm.setMeja(item);
					bdm.setId_rs(headerID);
					bdm.setNomor(nomorDtl);
					bdm.setTipe(tipeDtl);
					bdm.setKet(ketDtl);
				
				//Simpan per detil satu satu
				this.resvDtlService.insert(bdm);
			}
			
			result="berhasil";
			
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e.getMessage(), e);
			result ="gagal";
			
		}
		
		model.addAttribute("result",result);
		
		return "reservasi";
	}

	// list ex
	public String getIdExcludeList(List<DetailReservasiModel> rsvList) {
		String idExcludeList = "";
		String comma = "";
		if (rsvList.size() > 0) {
			for (int i = 0; i < rsvList.size(); i++) {
				if (i > 0) {
					comma = ",";
				} else {
					comma = " ";
				}

				int id = rsvList.get(i).getId_mj();
				idExcludeList = idExcludeList + comma + id;
			}
		}

		return idExcludeList;
	}

}
