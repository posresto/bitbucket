package com.xsis.bootcamp.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;


import com.xsis.bootcamp.model.ReturnDtlModel;
import com.xsis.bootcamp.model.SupplierModel;
import com.xsis.bootcamp.model.BahanModel;
import com.xsis.bootcamp.service.BahanService;
import com.xsis.bootcamp.service.SupplierService;

@Controller
public class ReturnController {
	private Log log = LogFactory.getLog(getClass());

	@Autowired
	private SupplierService supService;
	
	@Autowired
	private BahanService bhnService;

	List<ReturnDtlModel> rtrList = null;
	
	
		// HEADER //
		@RequestMapping(value="/return")
		public String index(Model model,HttpServletRequest request) {
			List<SupplierModel> supplier = null;
			rtrList = new ArrayList<ReturnDtlModel>();		
			
			try {
				supplier = this.supService.get();
			} catch (Exception e) {
				
			}
			
			model.addAttribute("listSupplier", supplier);
			
			return "return";
		}
		
		
		
		// DETAIL //
		
		
	// pilih Retur

	@RequestMapping(value ="/return/pilih_return")
	public String pilih_return(Model model, HttpServletRequest request,
			@RequestParam(value = "itemPilih") String[] itemIdPilih) throws Exception {

		for (int i = 0; i < itemIdPilih.length; i++) {
			itemIdPilih[i] = itemIdPilih[i].replace("[", "").replace("]", "").replace("\"", "");

			BahanMknModel im = new BahanMknModel();
			im = this.bhnService.getById(Integer.valueOf(itemIdPilih[i]));

			ReturnDtlModel jdm = new ReturnDtlModel();
			jdm.setId_bhn(im.getId());
			jdm.setBahan(im);
			
			
			rtrList.add(jdm);
		}
		model.addAttribute("rtrList", rtrList);
		model.addAttribute("success", "success");

		return "return";
	}

	// tambah Retur
	@RequestMapping(value ="/return/add")
	public String add(Model model) {
<<<<<<< HEAD

		List<BahanModel> listRetur = null;
=======
		List<BahanMknModel> bahan = null;
>>>>>>> 472a911c822097a6a968d805ff54ece6839f005c
		String idExcludeList = "";
		try {
			// object items diisi data dari method get
			
			bahan = this.bhnService.getByIdNotIn(idExcludeList);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}

		// datanya kita kirim ke view,
		// kita buat variable list kemudian diisi dengan object items
		model.addAttribute("listBahan", bahan);
		return "return/add";
	}

	// batal Retur
	@RequestMapping(value = "/return/batal_retur")
	public String batal_Retur(Model model, HttpServletRequest request) {

		int id = Integer.valueOf(request.getParameter("idx"));
		if (rtrList.size() > 0) {
			for (int i = 0; i < rtrList.size(); i++) {
				int idRetur = rtrList.get(i).getId_bhn();
				if (id == idRetur) {
					rtrList.remove(i);
				}
			}
		}

		model.addAttribute("rtrList", rtrList);
		model.addAttribute("success", "success");
		return "return";
	}

	// list ex
	public String getIdExcludeList(List<ReturnDtlModel> rsvList) {
		String idExcludeList = "";
		String comma = "";
		if (rsvList.size() > 0) {
			for (int i = 0; i < rsvList.size(); i++) {
				if (i > 0) {
					comma = ",";
				} else {
					comma = " ";
				}

				int id = rsvList.get(i).getId_bhn();
				idExcludeList = idExcludeList + comma + id;
			}
		}

		return idExcludeList;
	}
	
	
}
