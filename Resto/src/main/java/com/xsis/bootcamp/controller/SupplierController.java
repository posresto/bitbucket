package com.xsis.bootcamp.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xsis.bootcamp.model.SupplierModel;
import com.xsis.bootcamp.service.SupplierService;

@Controller
public class SupplierController {

	private Log log = LogFactory.getLog(getClass());

	@Autowired
	private SupplierService service;

	@RequestMapping(value = "/supplier")
	public String index(Model model) {

		return "supplier";
	}

	@RequestMapping(value = "/supplier/list")
	public String list(Model model) {
		// membuat object list dari class Fakultas model
		List<SupplierModel> items = null;

		try {
			// object items diisi data dari method get
			items = this.service.get();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}

		// datanya kita kirim ke view,
		// kita buat variable list kemudian diisi dengan object items
		model.addAttribute("list", items);

		return "supplier/list";
	}
	
	@RequestMapping(value = "/supplier/detail")
	public String detail(Model model, HttpServletRequest request) {
		// membuat object list dari class Fakultas model
		int id = Integer.parseInt(request.getParameter("id_sup"));

		SupplierModel item = null;

		try {
			item = this.service.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}

		model.addAttribute("item", item);

		return "supplier/detail";
	}

	@RequestMapping(value = "/supplier/add")
	public String add() {
		return "supplier/add";
	}

	// method untuk save data dari pop up (edit, insert, dan delet)
	@RequestMapping(value = "/supplier/save")
	public String save(Model model, @ModelAttribute SupplierModel item, HttpServletRequest request) {
		String proses = request.getParameter("proses");
		String result = "";

		try {
			if (proses.equals("insert")) {
				this.service.insert(item);
			} else if (proses.equals("update")) {
				this.service.update(item);
			} else if (proses.equals("delete")) {
				this.service.delete(item);
			}

			result = "berhasil";
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			result = "gagal";
		}
		model.addAttribute("result", result);

		return "supplier/save";
	}

	// method untuk memunculkan pop up delete
	@RequestMapping(value = "/supplier/edit")
	public String edit(Model model, HttpServletRequest request) {

		int id = Integer.parseInt(request.getParameter("id_sup"));

		SupplierModel item = null;

		try {
			item = this.service.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}

		model.addAttribute("item", item);
		return "supplier/edit";
	}

	// method untuk memunculkan pop up delete
	@RequestMapping(value = "/supplier/delete")
	public String delete(Model model, HttpServletRequest request) {

		int id = Integer.parseInt(request.getParameter("id_sup"));

		SupplierModel item = null;

		try {
			item = this.service.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}

		model.addAttribute("item", item);
		return "supplier/delete";
	}

}
