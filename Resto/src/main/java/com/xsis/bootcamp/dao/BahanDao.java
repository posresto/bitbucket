package com.xsis.bootcamp.dao;

import java.util.List;

import com.xsis.bootcamp.model.BahanModel;

public interface BahanDao {
	public List<BahanModel> get() throws Exception;
	public void insert(BahanModel model) throws Exception;
	public BahanModel getById(int id) throws Exception;
	public void update(BahanModel model) throws Exception;
	public void delete(BahanModel model) throws Exception;
	public List<BahanModel> getByIdNotIn(String idExcludeList) throws Exception;
}
