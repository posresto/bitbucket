package com.xsis.bootcamp.dao;

import com.xsis.bootcamp.model.PesanDtlModel;
import com.xsis.bootcamp.model.PesanHdrModel;

public interface PesanDao {

	public void insert(PesanHdrModel dhm) throws Exception;

	public void insert(PesanDtlModel dtm) throws Exception;
	
}
