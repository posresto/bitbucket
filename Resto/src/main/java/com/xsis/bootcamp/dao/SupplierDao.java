package com.xsis.bootcamp.dao;

import java.util.List;

import com.xsis.bootcamp.model.SupplierModel;

public interface SupplierDao {
	
	public List<SupplierModel> get() throws Exception;

	public SupplierModel getById(int id) throws Exception;
	
	public void insert(SupplierModel model) throws Exception;

	public void update(SupplierModel model) throws Exception;

	public void delete(SupplierModel model) throws Exception;
	
}
