package com.xsis.bootcamp.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.bootcamp.dao.BahanDao;
import com.xsis.bootcamp.model.BahanModel;

@Repository
public class BahanDaoImpl implements BahanDao {
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<BahanModel> get() throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		List<BahanModel> result = session.createQuery("from BahanModel b ORDER BY b.nama ASC").list();
		return result;
	}

	@Override
	public void insert(BahanModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
	}

	@Override
	public BahanModel getById(int id) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		BahanModel result = session.get(BahanModel.class, id);
		
		return result;
	}

	@Override
	public void update(BahanModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(model);
	}

	@Override
	public void delete(BahanModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(model);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<BahanModel> getByIdNotIn(String idExcludeList) throws Exception {
		String condition ="";
		if(!idExcludeList.isEmpty()) {
			condition = "where id NOT IN ("+idExcludeList+" ) ";
		}
		Session session = this.sessionFactory.getCurrentSession();
		List<BahanModel> result = session.createQuery("from BahanModel "+condition).list();
		return result;
	}

}