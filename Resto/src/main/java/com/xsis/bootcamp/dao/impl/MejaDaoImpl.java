package com.xsis.bootcamp.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.bootcamp.dao.MejaDao;
import com.xsis.bootcamp.model.MejaModel;

@Repository
public class MejaDaoImpl implements MejaDao {
	@Autowired
	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	@Override
	public List<MejaModel> get() throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		List<MejaModel> result = session.createQuery("from MejaModel m ORDER BY m.nomor ASC").list();
		return result;
	}

	@Override
	public void insert(MejaModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
	}

	@Override
	public MejaModel getById(int id) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		MejaModel result = session.get(MejaModel.class, id);

		return result;
	}

	@Override
	public void update(MejaModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(model);
	}

	@Override
	public void delete(MejaModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(model);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MejaModel> getByIdNotIn(String idExcludeList) throws Exception {
		// TODO Auto-generated method stub
		String condition ="";
		if(!idExcludeList.isEmpty()) {
			condition = "where id NOT IN ("+idExcludeList+" ) ";
		}
		Session session = this.sessionFactory.getCurrentSession();
		List<MejaModel> result = session.createQuery("from MejaModel "+condition).list();
		return result;
	}

}
