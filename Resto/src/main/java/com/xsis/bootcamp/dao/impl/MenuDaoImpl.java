package com.xsis.bootcamp.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.bootcamp.dao.MenuDao;
import com.xsis.bootcamp.model.MenuModel;

@Repository
public class MenuDaoImpl implements MenuDao {
	
	@Autowired
	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	@Override
	public List<MenuModel> get() throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		List<MenuModel> result = session.createQuery("from MenuModel").list();
		return result;
	}
	
	@Override
	public MenuModel getById(int id) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		MenuModel result = session.get(MenuModel.class, id);
		return result;
	}

	@Override
	public void insert(MenuModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
	}

	@Override
	public void update(MenuModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(model);
	}

	@Override
	public void delete(MenuModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(model);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MenuModel> getByIdNotIn(String idExcludeList) throws Exception {
		// TODO Auto-generated method stub
		String condition ="";
		if(!idExcludeList.isEmpty()) {
			condition = "where id NOT IN ("+idExcludeList+" ) ";
		}
		Session session = this.sessionFactory.getCurrentSession();
		List<MenuModel> result = session.createQuery("from MenuModel "+condition).list();
		return result;
	}

}
