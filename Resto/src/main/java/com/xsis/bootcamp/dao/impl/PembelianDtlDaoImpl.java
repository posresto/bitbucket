package com.xsis.bootcamp.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.bootcamp.dao.PembelianDtlDao;
import com.xsis.bootcamp.model.DetailPembelianModel;

@Repository
public class PembelianDtlDaoImpl implements PembelianDtlDao{

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<DetailPembelianModel> get() throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<DetailPembelianModel> result = session.createQuery("from DetailPembelianModel").list();
		return result;
	}

	@Override
	public void insert(DetailPembelianModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
		
	}

	@Override
	public DetailPembelianModel getById(int id) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		DetailPembelianModel result = session.get(DetailPembelianModel.class, id);
		
		return result;
	}

	@Override
	public void update(DetailPembelianModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(model);
	}

	@Override
	public void delete(DetailPembelianModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(model);
	}

}
