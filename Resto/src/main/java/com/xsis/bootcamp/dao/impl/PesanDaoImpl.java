package com.xsis.bootcamp.dao.impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.bootcamp.dao.PesanDao;
import com.xsis.bootcamp.model.PesanDtlModel;
import com.xsis.bootcamp.model.PesanHdrModel;

@Repository
public class PesanDaoImpl implements PesanDao {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void insert(PesanHdrModel dhm) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(dhm);
	}

	@Override
	public void insert(PesanDtlModel dtm) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(dtm);
	}

}
