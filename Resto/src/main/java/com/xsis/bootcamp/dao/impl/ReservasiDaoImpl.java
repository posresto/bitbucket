package com.xsis.bootcamp.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.bootcamp.dao.ReservasiDao;
import com.xsis.bootcamp.model.MejaModel;
import com.xsis.bootcamp.model.ReservasiModel;

@Repository
public class ReservasiDaoImpl implements ReservasiDao {
	@Autowired
	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	@Override
	public List<ReservasiModel> get() throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		List<ReservasiModel> result = session.createQuery("from ReservasiModel").list();
		return result;
	}

	@Override
	public void insert(ReservasiModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
	}

	@Override
	public ReservasiModel getById(int id) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		ReservasiModel result = session.get(ReservasiModel.class, id);

		return result;
	}

	@Override
	public void update(ReservasiModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(model);
	}

	@Override
	public void delete(ReservasiModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(model);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MejaModel> getSts(String status) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		List<MejaModel> result = session.createQuery("from MejaModel where status='"+status+"'").list();
		return result;
	}

}
