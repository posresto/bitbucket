package com.xsis.bootcamp.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.bootcamp.dao.ReservasiDtlDao;
import com.xsis.bootcamp.model.DetailReservasiModel;

@Repository
public class ReservasiDtlDaoImpl implements ReservasiDtlDao {
	@Autowired
	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	@Override
	public List<DetailReservasiModel> get() throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		List<DetailReservasiModel> result = session.createQuery("from DetailReservasiModel").list();
		return result;
	}

	@Override
	public void insert(DetailReservasiModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
	}

	@Override
	public DetailReservasiModel getById(int id) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		DetailReservasiModel result = session.get(DetailReservasiModel.class, id);

		return result;
	}

	@Override
	public void update(DetailReservasiModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(model);
	}

	@Override
	public void delete(DetailReservasiModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(model);
	}

}
