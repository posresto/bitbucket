package com.xsis.bootcamp.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.bootcamp.dao.SupplierDao;
import com.xsis.bootcamp.model.SupplierModel;

@Repository
public class SupplierDaoImpl implements SupplierDao {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SupplierModel> get() throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		List<SupplierModel> result = session.createQuery("from SupplierModel s ORDER BY s.nama ASC").list();
		return result;
	}

	@Override
	public SupplierModel getById(int id) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		SupplierModel result = session.get(SupplierModel.class, id);
		return result;
	}

	@Override
	public void insert(SupplierModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
		
	}

	@Override
	public void update(SupplierModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(model);
		
	}

	@Override
	public void delete(SupplierModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(model);
		
	}
	
	

}
