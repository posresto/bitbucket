package com.xsis.bootcamp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotBlank;


@Entity
@Table(name = "BAHAN_BAKU")
public class BahanModel {
	
	private Integer id;
	private String nama;
	private String jenis;
	private Integer stok;
	private String satuan;
		
	@Id
	@GeneratedValue
	@Column(name="ID_BAHAN")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="NAMA_BAHAN", unique=true)
	@NotBlank
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	
	@Column(name="JENIS_BAHAN")
	public String getJenis() {
		return jenis;
	}
	public void setJenis(String jenis) {
		this.jenis = jenis;
	}
	
	@Column(name="STOK_BAHAN")
	public Integer getStok() {
		return stok;
	}
	public void setStok(Integer stok) {
		this.stok = stok;
	}
	
	@Column(name="SATUAN_BAHAN")
	public String getSatuan() {
		return satuan;
	}
	public void setSatuan(String satuan) {
		this.satuan = satuan;
	}
	
}
