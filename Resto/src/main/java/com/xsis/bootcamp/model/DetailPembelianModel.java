package com.xsis.bootcamp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "DTL_BELI")
public class DetailPembelianModel {
	
	private Integer id;
	
	// id fk untuk bahan dan bli
	private Integer id_bhn;
	private String id_bli;
	
	private String nama_bhn;
	private Integer harga_beli;
	private String satuan;
	private Integer qty;
	private Integer subtotal_beli;
	
	private BahanModel bahan;
	private PembelianModel beli;
	
	
	
	
	@Id
	@GeneratedValue
	@Column(name="ID_DTL_BELI")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="ID_BAHAN")
	public Integer getId_bhn() {
		return id_bhn;
	}
	public void setId_bhn(Integer id_bhn) {
		this.id_bhn = id_bhn;
	}
	
	@Column(name="ID_BELI")
	public String getId_bli() {
		return id_bli;
	}
	public void setId_bli(String id_bli) {
		this.id_bli = id_bli;
	}
	
	@Column(name="HARGA_BELI")
	public Integer getHarga_beli() {
		return harga_beli;
	}
	public void setHarga_beli(Integer harga_beli) {
		this.harga_beli = harga_beli;
	}
	
	@Column(name="NAMA_BHN")
	public String getNama_bhn() {
		return nama_bhn;
	}
	public void setNama_bhn(String nama_bhn) {
		this.nama_bhn = nama_bhn;
	}
	
	@Column(name="QTY")
	public Integer getQty() {
		return qty;
	}
	public void setQty(Integer qty) {
		this.qty = qty;
	}
	
	@Column(name="SATUAN")
	public String getSatuan() {
		return satuan;
	}
	public void setSatuan(String satuan) {
		this.satuan = satuan;
	}
	
	@Column(name="SUB")
	public Integer getSubtotal_beli() {
		return subtotal_beli;
	}
	public void setSubtotal_beli(Integer subtotal_beli) {
		this.subtotal_beli = subtotal_beli;
	}
	
	
	@ManyToOne
	@JoinColumn(name="ID_BAHAN", nullable=false, updatable=false, insertable=false)
	public BahanModel getBahan() {
		return bahan;
	}
	public void setBahan(BahanModel bahan) {
		this.bahan = bahan;
	}
	
	@ManyToOne
	@JoinColumn(name="ID_BELI", nullable=false, updatable=false, insertable=false)
	public PembelianModel getBeli() {
		return beli;
	}
	public void setBeli(PembelianModel beli) {
		this.beli = beli;
	}
	
	
}
