package com.xsis.bootcamp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="MENU")
public class MenuModel {
	
	
	private Integer id_mkn;
	private String n_mkn;
	private Integer hrg_mkn;
	private String stock;
	private String jns_mkn;
	
	
	@Id
	@GeneratedValue
	@Column(name="ID_MENU")
	public Integer getId_mkn() {
		return id_mkn;
	}
	public void setId_mkn(Integer id_mkn) {
		this.id_mkn = id_mkn;
	}
	
	@Column(name="NAMA_MENU")
	public String getN_mkn() {
		return n_mkn;
	}
	
	public void setN_mkn(String n_mkn) {
		this.n_mkn = n_mkn;
	}
	
	@Column(name="HARGA_MENU")
	public Integer getHrg_mkn() {
		return hrg_mkn;
	}
	public void setHrg_mkn(Integer hrg_mkn) {
		this.hrg_mkn = hrg_mkn;
	}
	
	@Column(name="STOCK")
	public String getStock() {
		return stock;
	}
	public void setStock(String stock) {
		this.stock = stock;
	}
	
	@Column(name="JENIS_MENU")
	public String getJns_mkn() {
		return jns_mkn;
	}
	public void setJns_mkn(String jns_mkn) {
		this.jns_mkn = jns_mkn;
	}
	
	
	
}	
