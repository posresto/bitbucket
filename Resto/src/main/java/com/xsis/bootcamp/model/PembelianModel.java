package com.xsis.bootcamp.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotBlank;

@Entity
@Table(name = "PEMBELIAN")
public class PembelianModel {
	
	private String id;
	private String tgl;
	private String ket;
	private Integer total;
	private String tp_bayar;
	private Integer bayar;
	private Integer kembalian;
	
	private List<DetailPembelianModel> detail;
	
	
	// id untuk fk supplier
	private Integer id_sup;
	
	private SupplierModel sup;
	
	@Id
	@Column(name = "ID_BELI", unique=true)
	@NotBlank
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	@Column(name = "TGL_BELI")
	public String getTgl() {
		return tgl;
	}
	public void setTgl(String tgl) {
		this.tgl = tgl;
	}
	
	@Column(name = "KET_BELI")
	public String getKet() {
		return ket;
	}
	public void setKet(String ket) {
		this.ket = ket;
	}
	
	@Column(name="TOTAL")
	public Integer getTotal() {
		return total;
	}
	public void setTotal(Integer total) {
		this.total = total;
	}
	
	@Column(name="TIPE_BAYAR")
	public String getTp_bayar() {
		return tp_bayar;
	}
	public void setTp_bayar(String tp_bayar) {
		this.tp_bayar = tp_bayar;
	}
	
	@Column(name="BAYAR")
	public Integer getBayar() {
		return bayar;
	}
	public void setBayar(Integer bayar) {
		this.bayar = bayar;
	}
	
	@Column(name="KEMBALIAN")
	public Integer getKembalian() {
		return kembalian;
	}
	public void setKembalian(Integer kembalian) {
		this.kembalian = kembalian;
	}
	
	@Column(name="ID_SUP")
	public Integer getId_sup() {
		return id_sup;
	}
	public void setId_sup(Integer id_sup) {
		this.id_sup = id_sup;
	}
	
	@ManyToOne
	@JoinColumn(name="ID_SUP", nullable=false, updatable=false, insertable=false)
	public SupplierModel getSup() {
		return sup;
	}
	public void setSup(SupplierModel sup) {
		this.sup = sup;
	}
	
	@OneToMany(fetch=FetchType.EAGER, mappedBy="beli")
	public List<DetailPembelianModel> getDetail() {
		return detail;
	}
	public void setDetail(List<DetailPembelianModel> detail) {
		this.detail = detail;
	}

}
