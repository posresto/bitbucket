package com.xsis.bootcamp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="PESAN_DTL")
public class PesanDtlModel {
	
	private Integer id;
	private Integer id_ps;
	private Integer id_mn;
	private Integer qty;
	private MenuModel menu;
	private PesanHdrModel pesan;
	
	@Id
	@GeneratedValue
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="PESAN_HEADER")
	public Integer getId_ps() {
		return id_ps;
	}
	public void setId_ps(Integer id_ps) {
		this.id_ps = id_ps;
	}
	
	@Column(name="ID_MENU")
	public Integer getId_mn() {
		return id_mn;
	}
	public void setId_mn(Integer id_mn) {
		this.id_mn = id_mn;
	}
	
	@Column(name="QTY")
	public Integer getQty() {
		return qty;
	}
	public void setQty(Integer qty) {
		this.qty = qty;
	}
	
	
	@ManyToOne
	@JoinColumn(name="ID_MENU", nullable=false, updatable=false, insertable=false)
	public MenuModel getMenu() {
		return menu;
	}
	public void setMenu(MenuModel menu) {
		this.menu = menu;
	}
	
	
	@ManyToOne
	@JoinColumn(name="PESAN_HEADER", nullable=false, updatable=false, insertable=false)
	public PesanHdrModel getPesan() {
		return pesan;
	}
	public void setPesan(PesanHdrModel pesan) {
		this.pesan = pesan;
	}
}
