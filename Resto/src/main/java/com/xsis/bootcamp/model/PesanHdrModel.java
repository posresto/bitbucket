package com.xsis.bootcamp.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="PESAN_HDR")
public class PesanHdrModel {
	
	private Integer id;
	private Timestamp date;
	private String nama;
	private Integer id_mj;
	private MejaModel mj;
	
	
	@Id
	@GeneratedValue
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="TANGGAL")
	public Timestamp getDate() {
		return date;
	}
	public void setDate(Timestamp date) {
		this.date = date;
	}
	
	@Column(name="NAMA")
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	
	@Column(name="ID_MEJA")
	public Integer getId_mj() {
		return id_mj;
	}
	public void setId_mj(Integer id_mj) {
		this.id_mj = id_mj;
	}
	
	@ManyToOne
	@JoinColumn(name="ID_MEJA", nullable=false, insertable=false, updatable=false)
	public MejaModel getMj() {
		return mj;
	}
	public void setMj(MejaModel mj) {
		this.mj = mj;
	}
	
	
	
}
