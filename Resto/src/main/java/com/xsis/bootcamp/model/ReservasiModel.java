package com.xsis.bootcamp.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotBlank;

@Entity
@Table(name = "RESERVASI")
public class ReservasiModel {

	private Integer id;
	private String nama;
	private String hp;
	private String tgl;
	private String jam;
	private String ket;
	private List<DetailReservasiModel> detail;
	
	@Id
	@GeneratedValue
	@Column(name = "ID_RESV")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "NAMA_RSV")
	@NotBlank
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}

	@Column(name = "HP_RSV")
	public String getHp() {
		return hp;
	}
	public void setHp(String hp) {
		this.hp = hp;
	}

	@Column(name = "TGL_RSV")
	public String getTgl() {
		return tgl;
	}
	public void setTgl(String tgl) {
		this.tgl = tgl;
	}

	@Column(name = "JAM_RSV")
	public String getJam() {
		return jam;
	}
	public void setJam(String jam) {
		this.jam = jam;
	}

	@Column(name = "KET_RSV")
	public String getKet() {
		return ket;
	}
	public void setKet(String ket) {
		this.ket = ket;
	}

	@OneToMany(fetch=FetchType.EAGER, mappedBy="reservasi")
	public List<DetailReservasiModel> getDetail() {
		return detail;
	}
	public void setDetail(List<DetailReservasiModel> detail) {
		this.detail = detail;
	}
}
