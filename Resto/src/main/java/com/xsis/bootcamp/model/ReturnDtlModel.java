package com.xsis.bootcamp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="RETURN_DTL")
public class ReturnDtlModel {
	
	
		private Integer id;
		private Integer id_rtr;
		private Integer id_bhn;
		private Integer qty;
		private BahanModel bahan;
		private ReturnHdrModel retur;
		
		
		@Id
		@GeneratedValue
		public Integer getId() {
			return id;
		}
		public void setId(Integer id) {
			this.id = id;
		}
		
		@Column(name="RETURN_HEADER")
		public Integer getId_rtr() {
			return id_rtr;
		}
		public void setId_rtr(Integer id_rtr) {
			this.id_rtr = id_rtr;
		}
		
		@Column(name="ID_BAHAN")
		public Integer getId_bhn() {
			return id_bhn;
		}
		public void setId_bhn(Integer id_bhn) {
			this.id_bhn = id_bhn;
		}
		
		@Column(name="QTY")
		public Integer getQty() {
			return qty;
		}
		public void setQty(Integer qty) {
			this.qty = qty;
		}
		
		
		@ManyToOne
		@JoinColumn(name="ID_BAHAN", nullable=false, insertable=false, updatable=false)
		public BahanModel getBahan() {
			return bahan;
		}
		public void setBahan(BahanModel bahan) {
			this.bahan = bahan;
		}
		
		@ManyToOne
		@JoinColumn(name="RETURN_HEADER", nullable=false, insertable=false, updatable=false)
		public ReturnHdrModel getRetur() {
			return retur;
		}
		public void setRetur(ReturnHdrModel retur) {
			this.retur = retur;
		}
}
