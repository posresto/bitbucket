package com.xsis.bootcamp.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="RETURN_HDR")
public class ReturnHdrModel {

	
		private Integer id;
		private Timestamp tgl_rtr;
		private Integer id_sp;
		private SupplierModel sup;
		
		@Id
		@GeneratedValue
		public Integer getId() {
			return id;
		}
		public void setId(Integer id) {
			this.id = id;
		}
		
		@Column(name="TGL_RETUR")
		public Timestamp getTgl_rtr() {
			return tgl_rtr;
		}
		public void setTgl_rtr(Timestamp tgl_rtr) {
			this.tgl_rtr = tgl_rtr;
		}
		
		@Column(name="ID_SUP")
		public Integer getId_sp() {
			return id_sp;
		}
		public void setId_sp(Integer id_sp) {
			this.id_sp = id_sp;
		}
		
		@ManyToOne
		@JoinColumn(name="ID_SUP", nullable=false, insertable=false, updatable=false)
		public SupplierModel getSup() {
			return sup;
		}
		public void setSup(SupplierModel sup) {
			this.sup = sup;
		}
		
}
