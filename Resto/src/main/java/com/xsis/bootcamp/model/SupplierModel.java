package com.xsis.bootcamp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotBlank;

@Entity
@Table(name="SUPPLIER")
public class SupplierModel {
	

	private Integer id_sup;
	private String nama;
	private String tipe;
	private String email;
	private String telephon;
	
	@Id
	@GeneratedValue
	@Column(name="ID_SUP")
	public Integer getId_sup() {
		return id_sup;
	}
	public void setId_sup(Integer id_sup) {
		this.id_sup = id_sup;
	}
	
	
	@Column(name="NAMA_SUP", unique=true)
	@NotBlank
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	
	@Column(name="TIPE_SUP")
	public String getTipe() {
		return tipe;
	}
	public void setTipe(String tipe) {
		this.tipe = tipe;
	}
	
	@Column(name="EMAIL_SUP", unique=true)
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Column(name="TEL_SUP", unique=true)
	public String getTelephon() {
		return telephon;
	}
	public void setTelephon(String telephon) {
		this.telephon = telephon;
	}
	
	
}
