package com.xsis.bootcamp.service;

import java.util.List;

import com.xsis.bootcamp.model.MejaModel;

public interface MejaService {
	public List<MejaModel> get() throws Exception;
	
	public void insert(MejaModel model) throws Exception;

	public MejaModel getById(int id) throws Exception;

	public void update(MejaModel model) throws Exception;

	public void delete(MejaModel model) throws Exception;
	
	public List<MejaModel> getByIdNotIn(String idExcludeList) throws Exception;
	
	
}
