package com.xsis.bootcamp.service;

import java.util.List;

import com.xsis.bootcamp.model.MenuModel;

public interface MenuService {
	
	public List<MenuModel> get() throws Exception;
	
	public MenuModel getById(int id) throws Exception;

	public void insert(MenuModel model) throws Exception;

	public void update(MenuModel model) throws Exception;

	public void delete(MenuModel model) throws Exception;
	
	public List<MenuModel> getByIdNotIn(String idExcludeList) throws Exception;
	
}
