package com.xsis.bootcamp.service;

import com.xsis.bootcamp.model.PesanDtlModel;
import com.xsis.bootcamp.model.PesanHdrModel;

public interface PesanService {

	public void insert(PesanHdrModel dhm) throws Exception;

	public void insert(PesanDtlModel dtm) throws Exception;
	
}
