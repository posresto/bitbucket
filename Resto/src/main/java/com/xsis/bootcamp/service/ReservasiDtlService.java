package com.xsis.bootcamp.service;

import java.util.List;

import com.xsis.bootcamp.model.DetailReservasiModel;

public interface ReservasiDtlService {
	
	public List<DetailReservasiModel> get() throws Exception;

	public void insert(DetailReservasiModel model) throws Exception;

	public DetailReservasiModel getById(int id) throws Exception;

	public void update(DetailReservasiModel model) throws Exception;

	public void delete(DetailReservasiModel model) throws Exception;

}
