package com.xsis.bootcamp.service;

import java.util.List;

import com.xsis.bootcamp.model.MejaModel;
import com.xsis.bootcamp.model.ReservasiModel;

public interface ReservasiService {
	public List<ReservasiModel> get() throws Exception;
	
	public void insert(ReservasiModel model) throws Exception;

	public ReservasiModel getById(int id) throws Exception;

	public void update(ReservasiModel model) throws Exception;

	public void delete(ReservasiModel model) throws Exception;
	
	public List<MejaModel> getSts(String status) throws Exception;
	
}
