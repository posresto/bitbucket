package com.xsis.bootcamp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.bootcamp.dao.BahanDao;
import com.xsis.bootcamp.model.BahanModel;
import com.xsis.bootcamp.service.BahanService;

@Service
@Transactional
public class BahanServiceImpl implements BahanService {
	@Autowired
	private BahanDao dao;
	
	@Override
	public List<BahanModel> get() throws Exception {
		return this.dao.get();
	}

	@Override
	public void insert(BahanModel model) throws Exception {
		this.dao.insert(model);
	}

	@Override
	public BahanModel getById(int id) throws Exception {
		return this.dao.getById(id);
	}

	@Override
	public void update(BahanModel model) throws Exception {
		this.dao.update(model);
	}

	@Override
	public void delete(BahanModel model) throws Exception {
		this.dao.delete(model);		
	}

	@Override
	public List<BahanModel> getByIdNotIn(String idExcludeList) throws Exception {
		// TODO Auto-generated method stub
		return this.dao.getByIdNotIn(idExcludeList);
	}

}
