package com.xsis.bootcamp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.bootcamp.dao.MejaDao;
import com.xsis.bootcamp.model.MejaModel;
import com.xsis.bootcamp.service.MejaService;

@Service
@Transactional
public class MejaServiceImpl implements MejaService {
	@Autowired
	private MejaDao dao;

	@Override
	public List<MejaModel> get() throws Exception {
		return this.dao.get();
	}

	@Override
	public void insert(MejaModel model) throws Exception {
		this.dao.insert(model);
	}

	@Override
	public MejaModel getById(int id) throws Exception {
		return this.dao.getById(id);
	}

	@Override
	public void update(MejaModel model) throws Exception {
		this.dao.update(model);
	}

	@Override
	public void delete(MejaModel model) throws Exception {
		this.dao.delete(model);
	}

	@Override
	public List<MejaModel> getByIdNotIn(String idExcludeList) throws Exception {
		// TODO Auto-generated method stub
		return this.dao.getByIdNotIn(idExcludeList);
	}

}
