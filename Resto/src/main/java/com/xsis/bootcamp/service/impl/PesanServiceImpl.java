package com.xsis.bootcamp.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.bootcamp.dao.PesanDao;
import com.xsis.bootcamp.model.PesanDtlModel;
import com.xsis.bootcamp.model.PesanHdrModel;
import com.xsis.bootcamp.service.PesanService;

@Service
@Transactional
public class PesanServiceImpl implements PesanService{
	
	@Autowired
	private PesanDao dao;

	@Override
	public void insert(PesanHdrModel dhm) throws Exception {
		// TODO Auto-generated method stub
		this.dao.insert(dhm);
	}

	@Override
	public void insert(PesanDtlModel dtm) throws Exception {
		// TODO Auto-generated method stub
		this.dao.insert(dtm);
	}

}
