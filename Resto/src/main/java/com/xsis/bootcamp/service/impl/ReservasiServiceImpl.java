package com.xsis.bootcamp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.bootcamp.dao.ReservasiDao;
import com.xsis.bootcamp.model.MejaModel;
import com.xsis.bootcamp.model.ReservasiModel;
import com.xsis.bootcamp.service.ReservasiService;

@Service
@Transactional
public class ReservasiServiceImpl implements ReservasiService {
	@Autowired
	private ReservasiDao dao;

	@Override
	public List<ReservasiModel> get() throws Exception {
		return this.dao.get();
	}

	@Override
	public void insert(ReservasiModel model) throws Exception {
		this.dao.insert(model);
	}

	@Override
	public ReservasiModel getById(int id) throws Exception {
		return this.dao.getById(id);
	}

	@Override
	public void update(ReservasiModel model) throws Exception {
		this.dao.update(model);
	}

	@Override
	public void delete(ReservasiModel model) throws Exception {
		this.dao.delete(model);
	}

	@Override
	public List<MejaModel> getSts(String status) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
