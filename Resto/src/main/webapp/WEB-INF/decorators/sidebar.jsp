<aside class="main-sidebar">
	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar">
		<!-- Sidebar user panel -->
		<div class="user-panel">
			<div class="pull-left image">
				<img src="${contextName}/assets/dist/img/user2-160x160.jpg"
					class="img-circle" alt="User Image">
			</div>
			<div class="pull-left info">
				<p>${username}</p>
				<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
			</div>
		</div>
		<!-- search form -->
		
		<!-- /.search form -->
		<!-- sidebar menu: : style can be found in sidebar.less -->
		<ul class="sidebar-menu">
			<li class="header">MAIN NAVIGATION</li>
			<li class="treeview">
				<a href="#"> 
					<i	class="fa fa-dashboard"></i> <span>Master Data</span> 
					<span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i>	</span>
				</a>
				<ul class="treeview-menu">
					<li><a href="${contextName}/supplier.html" class="menu-item"><i class="fa fa-circle-o"></i> Supplier</a></li>
					<li><a href="${contextName}/menu.html" class="menu-item"><i class="fa fa-circle-o"></i> Menu</a></li>
					<li><a href="${contextName}/bahan.html" class="menu-item"><i class="fa fa-circle-o"></i> Bahan Baku</a></li>
					<li><a href="${contextName}/meja.html" class="menu-item"><i class="fa fa-circle-o"></i> Meja</a></li>
				</ul>
			</li>
			<li>
				<a href="#"> 
					<i	class="fa fa-laptop"></i> <span>Purchase</span> 
					<span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i>	</span>
				</a>
				<ul class="treeview-menu">
					<li><a href="${contextName}/pembelian.html"><i class="fa fa-circle-o"></i> Pembelian Bahan</a></li>
					<li><a href="${contextName}/return.html"><i class="fa fa-circle-o"></i> Pengembalian Bahan</a></li>
					<li><a href="${contextName}/listbeli.html"><i class="fa fa-circle-o"></i> List Pembelian Bahan</a></li>
					
				</ul>
			</li>
			
			<li>
				<a href="#"> 
					<i	class="fa fa-pie-chart"></i> <span>Transaksi</span> 
					<span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i>	</span>
				</a>
				<ul class="treeview-menu">
					<li><a href="${contextName}/pesan.html"><i class="fa fa-circle-o"></i> Pesanan</a></li>
					<li><a href="${contextName}/reservasi.html"><i class="fa fa-circle-o"></i> Reservasi</a></li>
					<li><a href="${contextName}/listrsv.html"><i class="fa fa-circle-o"></i> List Reservasi</a></li>
				</ul>
			</li>
			
		</ul>
	</section>
	<!-- /.sidebar -->
</aside>