<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<form id="form-bahan" method="post">
	<div class="form-horizontal">
		<input type="hidden" id="proses" name="proses" class="form-control" value="insert">
		
		<div class="form-group">
			<label class="control-label col-md-2">Nama Bahan</label>
			<div class="col-md-6">
				<input type="text" id="nama" name="nama" class="form-control" required="required">
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Jenis Bahan</label>
			<div class="col-md-6">
				<select id="jenis" name="jenis" required="required">
					<option value="">---Pilih---</option>
					<option value="Daging">Daging</option>
					<option value="Ikan">Ikan</option>
					<option value="Sayuran">Sayuran</option>
					<option value="Buah">Buah</option>
					<option value="Lain-lain">Lain-lain</option>
				</select>
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Stok</label>
			<div class="col-md-6">
				<input type="number" id="stok" name="stok" class="form-control" required="required" min="0" max="100">
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Satuan Bahan</label>
			<div class="col-md-6">
				<select id="satuan" name="satuan" required="required">
					<option value="">---Pilih---</option>
					<option value="Kg">Kg</option>
					<option value="Liter">Liter</option>
					<option value="Pcs">PCS</option>
					<option value="Dus">Dus</option>
					<option value="Lusin">Lusin</option>
				</select>
			</div>					
		</div>
		
		
	</div>

	<div class="modal-footer">
		<button type="submit" class="btn btn-success">Simpan</button>
	</div>
</form>