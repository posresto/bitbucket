<form id="form-bahan" method="post">
	<div class="form-horizontal">
		<input type="hidden" id="proses" name="proses" class="form-control" value="delete">
		<input type="hidden" id="id" name="id" class="form-control" value="${item.id}">
		
		<div class="form-group">
			<label class="control-label col-md-2">Nama Bahan</label>
			<div class="col-md-6">
				<input type="hidden" id="nama" name="nama" class="form-control" value="${item.nama}">
				<input type="text" id="dnama" name="dnama" class="form-control" value="${item.nama}" disabled="disabled">
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Jenis Bahan</label>
			<div class="col-md-6">
				<input type="hidden" id="jenis" name="jenis" class="form-control" value="${item.jenis}">
				<input type="text" id="djenis" name="djenis" class="form-control" value="${item.jenis}" disabled="disabled">
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Stok</label>
			<div class="col-md-6">
				<input type="hidden" id="stok" name="stok" class="form-control" value="${item.stok}">
				<input type="text" id="dstok" name="dstok" class="form-control" value="${item.stok}" disabled="disabled">
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Satuan Bahan</label>
			<div class="col-md-6">
				<input type="hidden" id="stok" name="stok" class="form-control" value="${item.satuan}">
				<input type="text" id="dstok" name="dstok" class="form-control" value="${item.satuan}" disabled="disabled">
			</div>					
		</div>
		
	</div>
	
</form>
