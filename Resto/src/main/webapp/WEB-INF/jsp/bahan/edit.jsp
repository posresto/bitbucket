<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<form id="form-bahan" method="post">
	<div class="form-horizontal">
		<input type="hidden" id="proses" name="proses" class="form-control" value="update">
				<input type="hidden" id="id" name="id" class="form-control" value="${item.id}">
		
		<div class="form-group">
			<label class="control-label col-md-2">Nama Bahan</label>
			<div class="col-md-6">
				<input type="text" id="nama" name="nama" class="form-control" value="${item.nama}">
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Jenis Bahan</label>
			<div class="col-md-6">
				<select id="jenis" name="jenis">
					<option value="Daging"
						<c:if test="${item.jenis == 'Daging'}">
							<c:out value="selected"/>
						</c:if>>Daging
					</option>
					
					<option value="Ikan"
						<c:if test="${item.jenis == 'Ikan'}">
							<c:out value="selected"/>
						</c:if>>Ikan
					</option>
					
					<option value="Sayuran"
						<c:if test="${item.jenis == 'Sayuran'}">
							<c:out value="selected"/>
						</c:if>>Sayuran
					</option>
					
					<option value="Buah"
						<c:if test="${item.jenis == 'Buah'}">
							<c:out value="selected"/>
						</c:if>>Buah
					</option>
					
					<option value="Lain-lain"
						<c:if test="${item.jenis == 'Lain-lain'}">
							<c:out value="selected"/>
						</c:if>>Lain-lain
					</option>
				</select>
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Stok</label>
			<div class="col-md-6">
				<input type="number" id="stok" name="stok" class="form-control" value="${item.stok}">
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Satuan Bahan</label>
			<div class="col-md-6">
				<select id="satuan" name="satuan">
					<option value="Kg"
						<c:if test="${item.satuan == 'Kg'}">
							<c:out value="selected"/>
						</c:if>>Kg
					</option>
					
					<option value="Liter"
						<c:if test="${item.satuan == 'Liter'}">
							<c:out value="selected"/>
						</c:if>>Liter
					</option>
					
					<option value="Pcs"
						<c:if test="${item.satuan == 'Pcs'}">
							<c:out value="selected"/>
						</c:if>>PCS
					</option>
					
					<option value="Dus"
						<c:if test="${item.satuan == 'Dus'}">
							<c:out value="selected"/>
						</c:if>>Dus
					</option>
					
					<option value="Lusin"
						<c:if test="${item.satuan == 'Lusin'}">
							<c:out value="selected"/>
						</c:if>>Lusin
					</option>
				</select>
			</div>					
		</div>
		
	</div>

	<div class="modal-footer">
		<button type="submit" class="btn btn-success">Simpan</button>
	</div>
</form>
