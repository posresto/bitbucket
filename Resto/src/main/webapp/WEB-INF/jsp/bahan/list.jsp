<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<c:forEach var="item" items="${list}">
	<tr>
		<td>${item.nama}</td>
		<td>${item.satuan}</td>
		<td>${item.stok}</td>
		<td>
			<button type="button" id="btn-edit" class="btn btn-success btn-xs btn-edit" value="${item.id }"><i class="fa fa-edit"></i></button>
			<button type="button" id="btn-delet" class="btn btn-danger btn-xs btn-delete" value="${item.id }"><i class="fa fa-trash"></i></button>
			<button type="button" id="btn-detail" class="btn btn-info btn-xs btn-detail" value="${item.id }"><i class="fa fa-eye"></i></button>
		</td>
	</tr>
</c:forEach>