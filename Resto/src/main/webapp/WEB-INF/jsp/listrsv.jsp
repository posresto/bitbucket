<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div class="box box-info">
	<div class="box-header">
		<h3 class="box-title">Data Reservasi</h3>
	</div>
	<div class="box-body">
		<table class="table">
			<thead>
				<tr>
					<th>Nama</th>
					<th>Tanggal</th>
					<th>Jam</th>
					<th>Hp</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody id="list-data">
			
			</tbody>
		</table>
	</div>
</div>

<!-- Modal -->
<div id="modal-input" class="modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-close"></i>
				</button>
				<h4>Detail Reservasi</h4>
			</div>
			<div class="modal-body">
			
			</div>
		</div>
	</div>
</div>

<script>

	// list data meja
	function loadData(){
		$.ajax({
			url:'reservasi/list.html',
			type:'get',
			dataType:'html',
			success:function(data){
				$("#list-data").html(data);
			}
		});
	}
	
	loadData();
	
	$(document).ready(function(){
		
		
		// button detail
		$("#list-data").on("click",".btn-detail",function(){
			var vId = $(this).val();
			$.ajax({
				url:'reservasi/detail.html',
				type:'get',
				data:{ id:vId },
				dataType:'html',
				success:function(data){
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');
				}
			});
		});
		
	});
</script>
