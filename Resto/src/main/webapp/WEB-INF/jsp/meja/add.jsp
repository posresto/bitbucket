<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<form id="form-meja" method="post">
	<div class="form-horizontal">
		<input type="hidden" id="proses" name="proses" class="form-control" value="insert">
		
		<div class="form-group">
			<label class="control-label col-md-2">Nomor Meja</label>
			<div class="col-md-6">
				<input type="text" id="nomor" name="nomor" pattern="[A-Z0-9]+" placeholder="With number or Capital Letter ex. 12A" class="form-control" required="required">
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Tipe Meja</label>
			<div class="col-md-6">
				<select id="tipe" name="tipe" required>
					<option value="">---Pilih Tipe---</option>
					<option value="Family">Family</option>
					<option value="Smoking-Date">Smoking-Date</option>
					<option value="Non-Smoking-Date">Non-Smoking-Date</option>
					<option value="smoking-Family">smoking-Family</option>
				</select>
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Status Meja</label>
			<div class="col-md-6">
				<input type="radio" name="status" value="Tersedia" required>Tersedia <br>
				<input type="radio" name="status" value="Tidak-tersedia" required>Tidak-tersedia
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Keterangan Meja</label>
			<div class="col-md-6">
				<input type="text" id="ket" name="ket" pattern="[a-zA-Z0-9- \.]+" class="form-control" required="required">
			</div>					
		</div>
		
	</div>

	<div class="modal-footer">
		<button type="submit" class="btn btn-success">Simpan</button>
	</div>
</form>
