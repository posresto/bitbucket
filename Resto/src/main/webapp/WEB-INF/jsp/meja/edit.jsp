<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<form id="form-meja" method="post">
	<div class="form-horizontal">
		<input type="hidden" id="proses" name="proses" class="form-control" value="update">
				<input type="hidden" id="id" name="id" class="form-control" value="${item.id}">
		
		<div class="form-group">
			<label class="control-label col-md-2">Nomor Meja</label>
			<div class="col-md-6">
				<input type="hidden" id="nomor" name="nomor" class="form-control" value="${item.nomor}">
				<input type="text" id="dnomor" name="dnomor" class="form-control" value="${item.nomor}" disabled="disabled">
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Tipe Meja</label>
			<div class="col-md-6">
				<select id="tipe" name="tipe">
					<option value="Family"
						<c:if test="${item.tipe == 'Family'}">
							<c:out value="selected"/>
						</c:if>>Family
					</option>
					<option value="Smoking-Date"
						<c:if test="${item.tipe == 'Smoking-Date'}">
							<c:out value="selected"/>
						</c:if>>Smoking-Date
					</option>
					<option value="Non-Smoking-Date"
						<c:if test="${item.tipe == 'Non-Smoking-Date'}">
							<c:out value="selected"/>
						</c:if>>Non-Smoking-Date
					</option>
					<option value="smoking-Family"
						<c:if test="${item.tipe == 'smoking-Family'}">
							<c:out value="selected"/>
						</c:if>>smoking-Family
					</option>
				</select>
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Status Meja</label>
			<div class="col-md-6">
				<input type="radio" name="status" value="Tersedia"
					<c:if test="${item.status == 'Tersedia'}">
						<c:out value="checked"/>
					</c:if>>Tersedia <br>
				<input type="radio" name="status" value="Tidak-tersedia"
					<c:if test="${item.status == 'Tidak-tersedia'}">
						<c:out value="checked"/>
					</c:if>>Tidak-tersedia
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Keterangan Meja</label>
			<div class="col-md-6">
				<input type="text" id="ket" name="ket" pattern="[a-zA-Z0-9- \.]+" class="form-control" value="${item.ket}">
			</div>					
		</div>
		
	</div>

	<div class="modal-footer">
		<button type="submit" class="btn btn-success">Simpan</button>
	</div>
</form>
