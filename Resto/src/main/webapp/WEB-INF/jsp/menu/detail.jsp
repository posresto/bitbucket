<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<a style="font-family: sans-serif; padding: 10px">DETAIL MENU</a>

<c:forEach var="item" items="${list}">
	<table style="padding: 10px">
	<tr>
		<td style="padding: 10px">ID</td>
		<td style="padding: 10px">  :</td>
		<td style="padding: 10px">${item.id_mkn}</td>
	</tr>
	<tr>
		<td style="padding: 10px">NAMA MENU</td>
		<td style="padding: 10px">  :</td>
		<td style="padding: 10px">${item.n_mkn}</td>
	</tr>
	<tr>
		<td style="padding: 10px">JENIS</td>
		<td style="padding: 10px">  :</td>
		<td style="padding: 10px">${item.jns_mkn}</td>
	<tr>
		<td style="padding: 10px">HARGA</td>
		<td style="padding: 10px">  :</td>
		<td style="padding: 10px">Rp. ${item.hrg_mkn},-</td>
	<tr>
		<td style="padding: 10px">STOCK</td>
		<td style="padding: 10px">  :</td>
		<td style="padding: 10px">${item.stock}</td>
	</tr>
	</table>
</c:forEach>