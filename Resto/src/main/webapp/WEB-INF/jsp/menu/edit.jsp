<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<form id="form-menu" action="update" method="post">
	<div class="form-horizontal">
		<input type="hidden" id="proses" name="proses" class="form-control"
			value="update">

		<div class="form-group">
			<label class="control-label col-md-2">ID</label>
			<div class="col-md-6">
				<input type="text" id="id_mkn" name="id_mkn" class="form-control" value="${item.id_mkn}" disabled="disabled">
				<input type="hidden" id="id_mkn" name="id_mkn" class="form-control" value="${item.id_mkn}">
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-md-2">Nama</label>
			<div class="col-md-6">
				<input type="text" id="n_mkn" name="n_mkn" class="form-control"
					value="${item.n_mkn}" required="required">
					<img id="img2" alt="" src="${pageContext.request.contextPath}/alert.png" style="display: none;">
				<a id="alt2" style="display: none;"><font color="red">Field nama harus diisi</font></a>
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-md-2">Jenis</label>
			<div class="col-md-6">
				<select name="jns_mkn">
					<option value ="Makanan"
					<c:if test ="${item.jns_mkn == 'Makanan'}">
						<c:out value ="selected"/>
					</c:if>>Makanan</option>
					
					<option value ="Minuman"
					<c:if test ="${item.jns_mkn == 'Minuman'}">
						<c:out value ="selected"/>
					</c:if>>Minuman</option>
					
					<option value ="Snack"
					<c:if test ="${item.jns_mkn == 'Snack'}">
						<c:out value ="selected"/>
					</c:if>>Snack</option>
					
					<option value ="Buah"
					<c:if test ="${item.jns_mkn == 'Buah'}">
						<c:out value ="selected"/>
					</c:if>>Buah</option>
				</select>
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-md-2">Harga</label>
			<div class="col-md-6">
				<input type="text" id="hrg_mkn" name="hrg_mkn" class="form-control"
					value="${item.hrg_mkn}">
					<img id="img3" alt="" src="${pageContext.request.contextPath}/alert.png" style="display: none;">
				<a id="alt3" style="display: none;"><font color="red">Field harga harus diisi</font></a>
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-md-2">Stock</label>
			<div class="col-md-6">
				<input type="radio" name="stock" value="Available"
				<c:if test="${item.stock == 'Available'}">
					<c:out value = "checked"/>
				</c:if>>Available 
				<br> 
				<input type="radio" name="stock" value="Sold Out"
				<c:if test = "${item.stock =='Sold Out'}">
					<c:out value="checked"/>
				</c:if>>Sold Out
			</div>
		</div>
		</div>

		<div class="modal-footer">
			<button type="submit" class="btn btn-success">Simpan</button>
		</div>
</form>
<script type="text/javascript">

function ceknama(){
		var nama = document.getElementById("n_mkn");
		if (nama.value == null || nama.value == "") {
			nama.style.borderColor = "red";
			document.getElementById("img2").style = "block";
			document.getElementById("alt2").style = "block";
		}
		else {
			nama.style.borderColor = "black";
			document.getElementById("img2").style.display = "none";
			document.getElementById("alt2").style.display = "none";
		}
	}
	
	function cekharga(){
		var stok = document.getElementById("hrg_mkn");
		if (stok.value == null || stok.value == "") {
			stok.style.borderColor = "red";
			document.getElementById("img3").style = "block";
			document.getElementById("alt3").style = "block";
		}
		else {
			stok.style.borderColor = "black";
			document.getElementById("img3").style.display = "none";
			document.getElementById("alt3").style.display = "none";
		}
	}
</script>