<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ page contentType="text/html"%>
<%@ page pageEncoding="UTF-8"%>
<%@ page import="java.util.*"%>
<%
	Date date = new Date();
%>


<form id="form-pembelian-simpan" method="post">
	<section class="forms">
		<div class="box box-info">
			<div class="box-header">
				<header>
					<h1 class="h3 display">FORM PEMBELIAN</h1>
				</header>
			</div>
			<div class="box-body">

				<div class="form-group">
					<label class="control-label col-md-4">No.Pembelian</label>
					<div class="col-md-6">
						<input type="text"  id="id" name="id"  placeholder="BL001" class="form-control" required="required">
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-4">Tanggal</label>
					<div class="col-md-6">
						<input type="date" id="tgl" name="tgl" class="form-control" required="required">
					</div>
				</div>
				
				<div class="form-group">
					<label class="control-label col-md-4">Supplier</label>
					<div class="col-md-6">
						<select id="id_sup" name="id_sup" class="form-control" required="required">
							<option value="">--- Pilih Supplier ---</option>
							<c:forEach var="sup" items="${listSupplier}">
								<option value="${sup.id_sup}">${sup.nama}</option>
							</c:forEach>
						</select>
					</div>
				</div>
				
				<div class="form-group">
					<label class="control-label col-md-4">Keterangan</label>
					<div class="col-md-6">
						<input type="text" id="ket" name="ket" class="form-control">
					</div>
				</div>

				<div class="form-group">
					<div class="box-tools" style="margin-left: 18px">
						<button type="button" id="btn-add"
							class="btn btn-primary pull-left">
							<i class="fa fa-plus"></i> Bahan
						</button>
					</div>
				</div>
			</div>
		</div>

		<div class="box box-info">
			<div class="box-body">
				<table class="table" id="tableDetail">
					<thead>
						<tr>
							<th></th>
							<th>Bahan</th>
							<th>Jumlah</th>
							<th>Satuan</th>
							<th>Harga Beli</th>
							<th>Sub-Total</th>
							<th>Hapus</th>
						</tr>
					</thead>

				</table>

				
			</div>
		</div>

		<div class="box box-info">
			<div class="box-body">

				<div class="form-horizontal">
					<input type="hidden" id="proses" name="proses" class="form-control"
						value="insert">
					<div class="form-group">
						<label class="control-label col-md-2">Total</label>
						<div class="col-md-6">
							<input type="hidden" id="total_jual" name="total_beli" class="form-control"> 
							<input type="text" id="total_jualdisplay" name="total_beli" class="form-control" disabled="disabled">
							<button type="button" id="btn-total" class="btn btn-primary pull-right" style="margin-top: 5px;" onclick="sumt();">
								<i class="fa fa-plus"></i> Hasil
							</button>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-2">Tipe Bayar</label>
						<div class="col-md-6">
							<select id="tipe_bayar" name="tipe_bayar" class="form-control" onchange="pilihBayar();">
								<option value="">Pilih</option>
								<option value="Tunai">Tunai</option>
								<option value="Debit">Debit</option>
								<option value="Kredit">Kredit</option>
							</select>
						</div>
					</div>
					
					<div class="form-group">
						<label class="control-label col-md-2">Bayar</label>
						<div class="col-md-6">
							<input type="hidden" id="bayar" name="bayar" pattern="[0-9]+" class="form-control" onkeyup="sum();">
							<input type="text" id="bayarDisplay" name="bayar" pattern="[0-9]+" class="form-control" disabled="disabled">
						</div>
					</div>
			

					<div class="form-group">
						<label class="control-label col-md-2">Kembalian</label>
						<div class="col-md-6">
							<input type="hidden" id="kembalian" name="kembalian"
								class="form-control"> <input type="text" id="kembalianDisplay"
								name="kembalianDisplay" class="form-control" disabled="disabled">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-2"></label>
						<div class="col-md-6">
							<div class="box-tools">
								<button type="submit" id="btn-bayar"
									class="btn btn-primary pull-right" style="display: none">
									<i class="fa fa-print"></i> Simpan
								</button>
							</div>
						</div>
					</div>

				</div>

			</div>
		</div>
	</section>
</form>

<!-- Modal -->
<div id="modal-input" class="modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-close"></i>
				</button>
				<h4>FORM PEMBELIAN</h4>
			</div>
			<div class="modal-body"></div>
		</div>
	</div>
</div>

<script>

function pilihBayar() {
	var tipe = $('#tipe_bayar').val();
	var total_jual = $('#total_jual').val();
	var nilaiReset = 0;
	var bayar = document.getElementById("bayar");
	var bayarDisplay = document.getElementById("bayarDisplay");
	var buttonSimpan = document.getElementById("btn-bayar");
	
	if(new String(tipe).valueOf() == new String("").valueOf()){
		buttonSimpan.style.display = "none";
		bayar.type = 'hidden';
		bayarDisplay.style.display = "block";
		
		$('#bayar').val(nilaiReset);
		$('#bayarDisplay').val(nilaiReset);
		$('#kembalian').val(nilaiReset);
		$('#kembalianDisplay').val(nilaiReset);
		
		alert("pilih tipe bayar dahulu");
	}else{
		buttonSimpan.style.display = "block";
		
		if(new String(tipe).valueOf() == new String("Tunai").valueOf()){
			bayar.type = 'text';
			bayarDisplay.style.display = "none";
		}else{
			bayar.type = 'hidden';
			bayarDisplay.style.display = "block";
			$('#bayar').val(total_jual);
			$('#bayarDisplay').val(total_jual);
			$('#kembalian').val(nilaiReset);
			$('#kembalianDisplay').val(nilaiReset);
		}
		 
	}
}





	//transaksi header
	function sum() {
		var bayar = document.getElementById('bayar').value;
		var total_jual = document.getElementById('total_jual').value;
		var result = parseInt(bayar) - parseInt(total_jual);
		if (!isNaN(result)) {
			document.getElementById('kembalian').value = result;
			document.getElementById('kembalianDisplay').value = result;
		}
	}

	//transaksi detail
	function sumx(index) {
		var hj = $("#hargabeli_" + index).val();
		var qty = $("#qty_" + index).val();
		var result = hj * qty;
		$("#hasil_" + index).val(result);
		$("#hasildisplay_" + index).val(result);
	}

	//transaksi total bayar
	function sumt() {
		var jD = $("#jumlahDetail").val();
		var total = 0;
		for (var i = 0; i < jD; i++) {
			var x = $("#hasil_" + i).val();
			total = parseInt(total) + parseInt(x);
		}
		$("#total_jual").val(total);
		$("#total_jualdisplay").val(total);
	}

	// fungsi hapus
	function namex(id) {
		var idx = id;
		$
				.ajax({
					url : 'pembelian/batal_beli.json',
					type : 'post',
					data : {
						idx : idx
					},

					success : function(data) {

						if (data.success) {

							$('#tableDetail').empty();
							$('#tableDetail').append(
									' ' + ' <tr> ' 
											+ '   <th></th> '
											+ '   <th></th> '
											+ '   <th>Bahan</th> '
											+ '   <th>Jumlah</th> '
											+ '   <th>Satuan</th> '
											+ '   <th>Harga Beli</th> '
											+ '   <th>Sub-Total</th> ' 
											+ '   <th>Hapus</th> ' 
										+ ' </tr> ');
							$
									.each(
											data.dpmList,
											function(index, jdt) {
												$('#tableDetail').append(
														' '
														+ ' <tr> '
														+ '  <td><input type="hidden"  id="jumlahDetail" name="jumlahDetail" value="'+data.dpmList.length+'"/></td> '
														+ '  <td><input type="hidden"  id="idBahan_'+index+'" name="idBahan_'+index+'" value="'+jdt.bahan.id+'"/></td> '
														+ '  <td><input type="hidden"  id="namaBhn_'+index+'" name="namaBhn_'+index+'" value="'+jdt.bahan.nama+'"/>'+jdt.bahan.nama+'</td> '
														+ '  <td><input type="text"    id="qty_'+index+'" name="qty_'+index+'" pattern="[0-9]+" style="width: 30px;" onkeyup="sumx('+ index + ');" /></td> '
														+ '  <td><input type="hidden"  id="satuan_'+index+'"  name="satuan_'+index+'"  value="'+jdt.bahan.satuan+'"/>' + jdt.bahan.satuan + '</td> '
														+ '  <td><input type="text" id="hargabeli_'+index+'" name="hargabeli_'+index+'" pattern="[0-9]+" onkeyup="sumx('+ index + ');" /></td> '
														+ '  <td><input type="hidden"  id="hasil_'+index+'" name="hasil_'+index+'"/><input type="text" id="hasildisplay_'+index+'" disabled="disabled"/></td> '
														+ '  <td><button type="button" class="btn btn-danger btn-xs btn-delete" value="" onclick="namex('+jdt.bahan.id+');"><i class="fa fa-trash"></i></button></td> '
														+ ' </tr> ');
											});

						}
					}
				});

	}

	// fungsi save seluruh tampilah jsp
	$(document)
			.ready(
					function() {

						
						// save function
						$("#form-pembelian-simpan").submit(function() {
							$.ajax({
								url : 'pembelian/simpan.json',
								type : 'post',
								data : $(this).serialize(), //SERIALIZE : UNTUK MENGAMBIL SELURUH NILAI YG DIINPUT PADA FORM
								dataType : 'json',
								success : function(data) {
									if(data.result=="berhasil"){
										alert("Data sudah berhasil di simpan");
									}
									else{
										alert("No.pembelian sudah ada");
									}
								}
							});
							
						});
						
						// add function
						$("#btn-add").on(
								"click",
								function() {
									$.ajax({
										url : 'pembelian/add',
										type : 'get',
										dataType : 'html',
										success : function(data) {
											$("#modal-input").find(
													".modal-body").html(data);
											$("#modal-input").modal('show');
										}
									});

								});

						

						// button Add Temporary
						$("#modal-input")
								.on(
										"submit",
										"#form-pembelian",
										function() {
											var bahanPilih = new Array();
											$('input[name="bahanPilih"]:checked')
													.each(
															function() {
																bahanPilih
																		.push(this.value);
															});
											var itemPilih = JSON
													.stringify(bahanPilih);
											console.log(itemPilih);
											$
													.ajax({
														url : 'pembelian/pilih_bahan.json',
														type : 'post',
														data : {
															itemPilih : itemPilih
														},
														dataType : 'json',
														success : function(data) {

															if (data.success) {
																$(
																		"#modal-input")
																		.modal(
																				'hide');
																$(
																		'#tableDetail')
																		.empty();
																$(
																		'#tableDetail')
																		.append(
																				' '
																						+ ' <tr> '
																						+ '   <th></th> '
																						+ '   <th></th> '
																						+ '   <th>Bahan</th> '
																						+ '   <th>Jumlah</th> '
																						+ '   <th>Satuan</th> '
																						+ '   <th>Harga Beli</th> '
																						+ '   <th>Sub-Total</th> ' 
																						+ '   <th>Hapus</th> ' 
																						+ ' </tr> ');
																$
																		.each(
																				data.dpmList,
																				function(
																						index,
																						jdt) {
																					$(
																							'#tableDetail')
																							.append(
																									' '
																									+ ' <tr> '
																									+ '  <td><input type="hidden"  id="jumlahDetail" name="jumlahDetail" value="'+data.dpmList.length+'"/></td> '
																									+ '  <td><input type="hidden"  id="idBahan_'+index+'" name="idBahan_'+index+'" value="'+jdt.bahan.id+'"/></td> '
																									+ '  <td><input type="hidden"  id="namaBhn_'+index+'" name="namaBhn_'+index+'" value="'+jdt.bahan.nama+'"/>'+jdt.bahan.nama+'</td> '
																									+ '  <td><input type="text"    id="qty_'+index+'" name="qty_'+index+'" pattern="[0-9]+" style="width: 30px;" onkeyup="sumx('+ index + ');" /></td> '
																									+ '  <td><input type="hidden"  id="satuan_'+index+'"  name="satuan_'+index+'"  value="'+jdt.bahan.satuan+'"/>' + jdt.bahan.satuan + '</td> '
																									+ '  <td><input type="text" id="hargabeli_'+index+'" name="hargabeli_'+index+'" pattern="[0-9]+" onkeyup="sumx('+ index + ');" /></td> '
																									+ '  <td><input type="hidden"  id="hasil_'+index+'" name="hasil_'+index+'"/><input type="text" id="hasildisplay_'+index+'" disabled="disabled"/></td> '
																									+ '  <td><button type="button" class="btn btn-danger btn-xs btn-delete" value="" onclick="namex('+jdt.bahan.id+');"><i class="fa fa-trash"></i></button></td> '
																									+ ' </tr> ');
																				});
															}

														}

													});
											return false;
										});

					});
</script>
