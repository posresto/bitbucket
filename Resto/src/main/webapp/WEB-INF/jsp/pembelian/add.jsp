<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<form id="form-pembelian" method="post">
	
	<input type="hidden" id="proses" name="proses" class="form-control"
		value="insert">

	<div class="form-group">
		<table class="table">
			<thead>
				<tr>
					<th>No.Meja</th>
					<th>Satuan</th>
					<th>Stok</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<c:choose>
					<c:when test="${listBahan.size()>0}">
						<c:forEach var="list" items="${listBahan}">
							<tr>
								<td>${list.nama}</td>
								<td>${list.stok}</td>
								<td>${list.satuan}</td>
								<td><input type="checkbox" name="bahanPilih"
									value="${list.id}" /></td>
							</tr>
						</c:forEach>
					</c:when>
					<c:otherwise>
						<tr>
							<td colspan="4">Bahan Kosong</td>
						</tr>
					</c:otherwise>

				</c:choose>

			</tbody>
		</table>
	</div>

	<c:choose>
		<c:when test="${listBahan.size()>0}">
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary pull-left">Simpan</button>
			</div>
		</c:when>
	</c:choose>

</form>
