<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ page contentType="text/html"%>
<%@ page pageEncoding="UTF-8"%>
<%@ page import="java.util.*"%>
<%
	Date date = new Date();
%>


<form id="form-pesan-cetak" method="post">
	<section class="forms">
		<div class="box box-info">
			<div class="box-header">
				<header>
					<h1 class="h3 display">FORM PEMESANAN</h1>
				</header>
			</div>
			<div class="box-body">

				<div class="form-group">
					<label class="control-label col-md-4">Pemesan</label>
					<div class="col-md-6">
						<input type="text" id="n_psn" name="n_psn" class="form-control">
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-4">Meja</label>
					<div class="col-md-6">
						<select id="id_meja" name="id_meja" class="form-control">
							<c:forEach var="meja" items="${listMeja}">
								<option value="${meja.id}">${meja.nomor}-${meja.tipe}</option>
							</c:forEach>
						</select>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-4">Tanggal</label>
					<div class="col-md-6">
						<input type="text" id="tgl_psn" name="tgl_psn"
							class="form-control" value="<%=date.toString()%>"
							disabled="disabled">
					</div>
				</div>

				<div class="form-group">
					<div class="box-tools" style="margin-left: 18px">
						<button type="button" id="btn-add"
							class="btn btn-primary pull-left">
							<i class="fa fa-plus"></i> Tambah Pesanan
						</button>
					</div>
				</div>
			</div>
		</div>

		<div class="box box-info">
			<div class="box-body">
				<table class="table" id="tableDetail">
					<thead>
						<tr>
							<th></th>
							<th>Menu</th>
							<th>Jumlah</th>
							<th>Harga</th>
							<th>Sub-Total</th>
						</tr>
					</thead>

				</table>

				
			</div>
		</div>

		<div class="box box-info">
			<div class="box-body">

				<div class="form-horizontal">
					<input type="hidden" id="proses" name="proses" class="form-control"
						value="insert">
					<div class="form-group">
						<label class="control-label col-md-2">Total</label>
						<div class="col-md-6">
							<input type="hidden" id="total_jual" name="total_jual"
								class="form-control"> <input type="text"
								id="total_jualdisplay" name="total_jual" class="form-control"
								disabled="disabled">
							<button type="button" id="btn-total"
								class="btn btn-primary pull-right" style="margin-top: 5px;"
								onclick="sumt();">
								<i class="fa fa-plus"></i> Hasil
							</button>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-2">Tipe Bayar</label>
						<div class="col-md-6">
							<select id="tipe_bayar" name="tipe_bayar" class="form-control" onchange="pilihBayar();">
								<option value="">Pilih</option>
								<option value="Tunai">Tunai</option>
								<option value="Debit">Debit</option>
								<option value="Kredit">Kredit</option>
							</select>
						</div>
					</div>
					
					<div class="form-group">
						<label class="control-label col-md-2">Bayar</label>
						<div class="col-md-6">
							<input type="hidden" id="bayar" name="bayar" class="form-control" onkeyup="sum();">
							<input type="text" id="bayarDisplay" name="bayar" class="form-control" disabled="disabled">
						</div>
					</div>
			

					<div class="form-group">
						<label class="control-label col-md-2">Kembalian</label>
						<div class="col-md-6">
							<input type="hidden" id="kembalian" name="kembalian"
								class="form-control"> <input type="text" id="kembalianDisplay"
								name="kembalianDisplay" class="form-control" disabled="disabled">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-2"></label>
						<div class="col-md-6">
							<div class="box-tools">
								<button type="submit" id="btn-bayar"
									class="btn btn-primary pull-right" style="display: none">
									<i class="fa fa-print"></i> Bill
								</button>
							</div>
						</div>
					</div>

				</div>

			</div>
		</div>
	</section>
</form>

<!-- Modal -->
<div id="modal-input" class="modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-close"></i>
				</button>
				<h4>FORM PEMESANAN</h4>
			</div>
			<div class="modal-body"></div>
		</div>
	</div>
</div>

<script>

function pilihBayar() {
	var tipe = $('#tipe_bayar').val();
	var total_jual = $('#total_jual').val();
	var nilaiReset = 0;
	var bayar = document.getElementById("bayar");
	var bayarDisplay = document.getElementById("bayarDisplay");
	var buttonSimpan = document.getElementById("btn-bayar");
	
	if(new String(tipe).valueOf() == new String("").valueOf()){
		buttonSimpan.style.display = "none";
		bayar.type = 'hidden';
		bayarDisplay.style.display = "block";
		
		$('#bayar').val(nilaiReset);
		$('#bayarDisplay').val(nilaiReset);
		$('#kembalian').val(nilaiReset);
		$('#kembalianDisplay').val(nilaiReset);
		
		alert("pilih tipe bayar dahulu");
	}else{
		buttonSimpan.style.display = "block";
		
		if(new String(tipe).valueOf() == new String("Tunai").valueOf()){
			bayar.type = 'text';
			bayarDisplay.style.display = "none";
		}else{
			bayar.type = 'hidden';
			bayarDisplay.style.display = "block";
			$('#bayar').val(total_jual);
			$('#bayarDisplay').val(total_jual);
			$('#kembalian').val(nilaiReset);
			$('#kembalianDisplay').val(nilaiReset);
		}
		 
	}
}





	//transaksi header
	function sum() {
		var bayar = document.getElementById('bayar').value;
		var total_jual = document.getElementById('total_jual').value;
		var result = parseInt(bayar) - parseInt(total_jual);
		if (!isNaN(result)) {
			document.getElementById('kembalian').value = result;
			document.getElementById('kembalianDisplay').value = result;
		}
	}

	//transaksi detail
	function sumx(index) {
		var hj = $("#hargajual_" + index).val();
		var qty = $("#qty_" + index).val();
		var result = hj * qty;
		$("#hasil_" + index).val(result);
		$("#hasildisplay_" + index).val(result);
	}

	//transaksi total bayar
	function sumt() {
		var jD = $("#jumlahDetail").val();
		var total = 0;
		for (var i = 0; i < jD; i++) {
			var x = $("#hasil_" + i).val();
			total = parseInt(total) + parseInt(x);
		}
		$("#total_jual").val(total);
		$("#total_jualdisplay").val(total);
	}

	function namex(id) {
		var idx = id;
		$
				.ajax({
					url : 'pesan/batal_pesan.json',
					type : 'post',
					data : {
						idx : idx
					},

					success : function(data) {

						if (data.success) {

							$('#tableDetail').empty();
							$('#tableDetail').append(
									' ' + ' <tr> ' 
											+ '   <th></th> '
											+ '   <th>Menu</th> '
											+ '   <th>Jumlah</th> '
											+ '   <th>Harga</th> '
											+ '   <th>Sub-Total</th> ' + ' </tr> ');
							$
									.each(
											data.pdmList,
											function(index, jdt) {
												$('#tableDetail')
														.append(
																' '
																		+ ' <tr> '
																		+ '   <td><input type="hidden"  id="jumlahDetail" value="'+data.pdmList.length+'"/>'
																		+ '</td> '
																		+ '   <td>'
																		+ jdt.menu.n_mkn
																		+ '</td> '
																		+ '   <td>'
																		+ jdt.menu.hrg_mkn
																		+ '<input type="hidden" id="hargajual_'+index+'" value="'+jdt.menu.hrg_mkn+'"/></td> '
																		+ '   <td><input type="text"  id="qty_'
																		+ index
																		+ '" style="width: 30px;" onkeyup="sumx('
																		+ index
																		+ ');"/></td> '
																		+ '   <td><input type="hidden"  id="hasil_'+index+'"/><input type="text" id="hasildisplay_'+index+'" disabled="disabled"/></td> '
																		+ '   <td><button type="button" class="btn btn-danger btn-xs btn-delete" onclick="namex('+jdt.menu.id_mkn+')"><i class="fa fa-trash"></i></button></td> '
																		+ ' </tr> ');
											});

						}
					}
				});

	}

	$(document).ready(function() {

						// add function
						$("#btn-add").on(
								"click",
								function() {
									$.ajax({
										url : 'pesan/add.html',
										type : 'get',
										dataType : 'html',
										success : function(data) {
											$("#modal-input").find(
													".modal-body").html(data);
											$("#modal-input").modal('show');
										}
									});

								});

						// BUTTON SAVE
						$("#form-pesan-cetak").submit(function() {
							$.ajax({
								url : 'pesan-cetak.json',
								type : 'post',
								data : $(this).serialize(), //SERIALIZE : UNTUK MENGAMBIL SELURUH NILAI YG DIINPUT PADA FORM
								dataType : 'json',
								success : function(data) {
								}
							});
						});

						// button Add Temporary
						$("#modal-input")
								.on(
										"submit",
										"#form-pesan",
										function() {
											var menuPilih = new Array();
											$('input[name="menuPilih"]:checked')
													.each(
															function() {
																menuPilih
																		.push(this.value);
															});
											var itemPilih = JSON
													.stringify(menuPilih);
											console.log(itemPilih);
											$
													.ajax({
														url : 'pesan/pilih_menu.json',
														type : 'post',
														data : {
															itemPilih : itemPilih
														},
														dataType : 'json',
														success : function(data) {

															if (data.success) {
																$(
																		"#modal-input")
																		.modal(
																				'hide');
																$(
																		'#tableDetail')
																		.empty();
																$(
																		'#tableDetail')
																		.append(
																				' '
																						+ ' <tr> '
																						+ '   <th></th> '
																						+ '   <th>Menu</th> '
																						+ '   <th>Jumlah</th> '
																						+ '   <th>Harga</th> '
																						+ '   <th>Sub-Total</th> '
																						+ ' </tr> ');
																$
																		.each(
																				data.pdmList,
																				function(
																						index,
																						jdt) {
																					$(
																							'#tableDetail')
																							.append(
																									' '
																											+ ' <tr> '
																											+ '   <td><input type="hidden"  id="jumlahDetail" value="'+data.pdmList.length+'"/>'
																											+ '</td> '
																											+ '   <td>'
																											+ jdt.menu.n_mkn
																											+ '</td> '
																											+ '   <td>'
																											+ jdt.menu.hrg_mkn
																											+ '<input type="hidden" id="hargajual_'+index+'" value="'+jdt.menu.hrg_mkn+'"/></td> '
																											+ '   <td><input type="text"  id="qty_'
																											+ index
																											+ '" style="width: 30px;" onkeyup="sumx('
																											+ index
																											+ ');"/></td> '
																											+ '   <td><input type="hidden"  id="hasil_'+index+'"/><input type="text" id="hasildisplay_'+index+'" disabled="disabled"/></td> '
																											+ '   <td><button type="button" class="btn btn-danger btn-xs btn-delete" onclick="namex('+jdt.menu.id_mkn+')"><i class="fa fa-trash"></i></button></td> '
																											+ ' </tr> ');
																				});
															}

														}

													});
											return false;
										});

					});
</script>
