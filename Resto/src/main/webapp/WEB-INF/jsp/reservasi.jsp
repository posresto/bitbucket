<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ page contentType="text/html" %>
<%@ page pageEncoding="UTF-8" %>
<%@ page import = "java.util.*"  %>
<% Date date = new Date(); %>

<form id="form-reservasi-simpan" method="post">
	<section class="forms">
		<div class="box box-info">
			<div class="box-header">
				<header>
					<h1 class="h3 display">Reservasi Meja</h1>
				</header>
			</div>
			<div class="box-body">
			
				<div class="form-group">
					<label class="control-label col-md-4">Nama</label>
					<div class="col-md-6">
						<input type="text" id="nama" name="nama" class="form-control" required="required">
					</div>
				</div>
				
				<div class="form-group">
					<label class="control-label col-md-4">Hp</label>
					<div class="col-md-6">
						<input type="tel" id="hp" name="hp" pattern="^\d{12}$" class="form-control" required="required">
					</div>
				</div>
	
				<div class="form-group">
					<label class="control-label col-md-4">Tanggal</label>
					<div class="col-md-6">
						<input type="date" id="tgl" name="tgl" class="form-control" required="required">
					</div>
				</div>
	
				<div class="form-group">
					<label class="control-label col-md-4">Jam</label>
					<div class="col-md-6">
						<input type="time" id="jam" name="jam" class="form-control" required="required">
					</div>
				</div>
				
				<div class="form-group">
					<label class="control-label col-md-4">Keterangan</label>
					<div class="col-md-6">
						<input type="text" id="ket" name="ket" class="form-control" required="required">
					</div>
				</div>
				
				<div class="box-tools" style="margin-left: 18px">
					<button type="button" id="btn-add" class="btn btn-primary pull-left" ><i class="fa fa-plus"></i> Reservasi</button>
				</div>
	
			</div>
		</div>
		
		<div class="box box-info">
			<div class="box-body">
			<table class="table" id="tableDetail">
				<thead>
					<tr>
						<th>No.Meja</th>
						<th>Tipe</th>
						<th>Ket</th>
						<th>Action</th>
					</tr>
				</thead>
				
			</table>
			</div>
		</div>
		
			<div class="box-body">
				<div class="box-tools" style="margin-left: 18px">
					<button type="submit" id="btn-cetak" class="btn btn-primary pull-left"><i class="fa fa-print"></i> Simpan</button>
				</div>
			</div>
		
	</section>
</form>

<!-- Modal -->
<div id="modal-input" class="modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-close"></i>
				</button>
				<h4>Form Reservasi</h4>
			</div>
			<div class="modal-body">
			 
			</div>
		</div>
	</div>
</div>

<script>

	// sungsi menghapus meja pada list
	function namex(id) {
		var idx = id;
		$.ajax({
			url:'reservasi/batal_meja.json',
			type:'post',
			data: { idx : idx},
			
			success:function(data){
			
				if(data.success){
					
					$('#tableDetail').empty();
					$('#tableDetail').append(' '
							+' <tr> ' 
							+'   <th></th> '
							+'   <th></th> '
							+'   <th>No.Meja</th> '
					        +'   <th>Tipe</th> '
					        +'   <th>Ket</th> '
					        +'   <th>Action</th> '
					        +' </tr> ');
					$.each(data.rsvList, function(index,jdt){
						$('#tableDetail').append(' ' 
								+' <tr> ' 
								+ '  <td><input type="hidden"  id="jumlahDetail" name="jumlahDetail" value="'+data.rsvList.length+'"/></td> '
								+ '  <td><input type="hidden"  id="idMeja_'+index+'" name="idMeja_'+index+'" value="'+jdt.meja.id+'"/></td> '
								+'   <td><input type="hidden"  id="nomor_'+index+'" name="nomor_'+index+'" value="'+jdt.meja.nomor+'"/>'+jdt.meja.nomor+'</td> '
						        +'   <td><input type="hidden"  id="tipe_'+index+'" name="tipe_'+index+'" value="'+jdt.meja.tipe+'"/>'+jdt.meja.tipe+'</td> '
						        +'   <td><input type="hidden"  id="ket_'+index+'" name="ket_'+index+'" value="'+jdt.meja.ket+'"/>'+jdt.meja.ket+'</td> '
						        +'   <td><button type="button" id="btn-batal"  class="btn btn-danger btn-xs btn-delete" onclick="namex('+jdt.meja.id+');"><i class="fa fa-trash"></i></button></td> '
						        +' </tr> ');
					});
					
				}
			}
		});
		
	}
	
	$(document).ready(function(){
		
		// fungsi menempilkan pop up  meja tambah
		$("#btn-add").on("click",function(){
			$.ajax({
				url:'reservasi/add.html',
				type:'get',
				dataType:'html',
				success:function(data){
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');
				}
			});
			
		});
		
		// save function
		$("#form-reservasi-simpan").submit(function() {
			$.ajax({
				url : 'reservasi/simpan.json',
				type : 'post',
				data : $(this).serialize(), //SERIALIZE : UNTUK MENGAMBIL SELURUH NILAI YG DIINPUT PADA FORM
				dataType : 'json',
				success : function(data) {
					if(data.result=="berhasil"){
						alert("Data berhasil di simpan");
					}
					else{
						alert("No.Pembelian sudah ada");
					}
				}
			});
			
		});
		
		// button tambah ke list
		$("#modal-input").on("submit","#form-meja",function(){
			var mejaPilih = new Array();
			$('input[name="mejaPilih"]:checked').each(function() {
				mejaPilih.push(this.value);
					});
			var itemPilih = JSON.stringify(mejaPilih);
			console.log(itemPilih);
		$.ajax({
			url:'reservasi/pilih_meja.json',
			type:'post',
			data: { itemPilih : itemPilih},
			dataType:'json',
			success:function(data){

				if(data.success){
					$("#modal-input").modal('hide');
					$('#tableDetail').empty();
					$('#tableDetail').append(' '
							+' <tr> ' 
							+'   <th></th> '
							+'   <th></th> '
							+'   <th>No.Meja</th> '
					        +'   <th>Tipe</th> '
					        +'   <th>Ket</th> '
					        +'   <th>Action</th> '
					        +' </tr> ');
					$.each(data.rsvList, function(index,jdt){
						$('#tableDetail').append(' ' 
								+' <tr> ' 
								+ '  <td><input type="hidden"  id="jumlahDetail" name="jumlahDetail" value="'+data.rsvList.length+'"/></td> '
								+ '  <td><input type="hidden"  id="idMeja_'+index+'" name="idMeja_'+index+'" value="'+jdt.meja.id+'"/></td> '
								+'   <td><input type="hidden"  id="nomor_'+index+'" name="nomor_'+index+'" value="'+jdt.meja.nomor+'"/>'+jdt.meja.nomor+'</td> '
						        +'   <td><input type="hidden"  id="tipe_'+index+'" name="tipe_'+index+'" value="'+jdt.meja.tipe+'"/>'+jdt.meja.tipe+'</td> '
						        +'   <td><input type="hidden"  id="ket_'+index+'" name="ket_'+index+'" value="'+jdt.meja.ket+'"/>'+jdt.meja.ket+'</td> '
						        +'   <td><button type="button" id="btn-batal"  class="btn btn-danger btn-xs btn-delete" onclick="namex('+jdt.meja.id+');"><i class="fa fa-trash"></i></button></td> '
						        +' </tr> ');
					});
					
				}
				
				
			}
			
		});
			return false;
		});
		
	});
</script>
