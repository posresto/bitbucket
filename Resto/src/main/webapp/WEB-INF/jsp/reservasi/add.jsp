<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<form id="form-meja" method="post">
	<input type="hidden" id="proses" name="proses" class="form-control"
		value="insert">

	<div class="form-group">
		<table class="table">
			<thead>
				<tr>
					<th>No.Meja</th>
					<th>Tipe</th>
					<th>ket</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<c:choose>
					<c:when test="${listMeja.size()>0}">
						<c:forEach var="list" items="${listMeja}">
							<tr>
								<td>${list.nomor}</td>
								<td>${list.tipe}</td>
								<td>${list.ket}</td>
								<td><input type="checkbox" name="mejaPilih"
									value="${list.id}" /></td>
							</tr>
						</c:forEach>
					</c:when>
					
					<c:otherwise>
						<tr>
							<td colspan="4">Meja Kosong</td>
						</tr>
					</c:otherwise>

				</c:choose>

			</tbody>
		</table>
	</div>

	<c:choose>
		<c:when test="${listMeja.size()>0}">
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary pull-left">Simpan</button>
			</div>
		</c:when>


	</c:choose>


</form>
