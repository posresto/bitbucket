<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Detail Reservasi</title>
</head>
<body>


<div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Reservasi</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table class="table table-striped">
                <tbody>
                <tr>
                <td>
						Nama
				</td>
					<td>
						${item.nama}
					</td>
				</tr>
				<tr>
					<td>
						HP
					</td>
					<td>
						${item.hp}
					</td>
				</tr>
				<tr>
					<td>
						Tanggal
					</td>
					<td>
						${item.tgl}
					</td>
				</tr>
				<tr>
					<td>
						Jam
					</td>
					<td>
						${item.jam}
					</td>
				</tr>
				</tbody>
			  </table>
			</div>
			
			<div class="form-group">
			<table class="table">
			<thead>
				<tr>
					<th>No.Meja</th>
					<th>Tipe</th>
					<th>Keterangan</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="itemDetail" items="${item.detail}">
					<tr>
						<td>${itemDetail.nomor}</td>
						<td>${itemDetail.tipe}</td>
						<td>${itemDetail.ket}</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		</div>
	<br>
	<table>
	<tr>
		<td colspan="2">
		<!-- Kembali ke list -->
		<a class="btn btn-block btn-danger" href="listrsv.html">Back</a>
		</td>
	</tr>
</table>
</div>
</body>
</html>