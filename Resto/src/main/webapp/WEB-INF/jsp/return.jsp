<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ page contentType="text/html"%>
<%@ page pageEncoding="UTF-8"%>
<%@ page import="java.util.*"%>
<%
	Date date = new Date();
%>


<form id="form-return-cetak" method="post">
	<section class="forms">
		<div class="box box-info">
			<div class="box-header">
				<header>
					<h1 class="h3 display">FORM PENGEMBALIAN</h1>
				</header>
			</div>
			<div class="box-body">

				<div class="form-group">
					<label class="control-label col-md-4">TGL Kembali</label>
					<div class="col-md-6">
						<input type="date" id="tgl_rtr" name="tgl_rtr" class="form-control">
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-4">Supplier</label>
					<div class="col-md-6">
						<select id="id_sup" name="id_sup" class="form-control">
							<c:forEach var="supplier" items="${listSupplier}">
								<option value="${supplier.id_sup}">${supplier.nama}</option>
							</c:forEach>
						</select>
					</div>
				</div>

				<div class="form-group">
					<div class="box-tools" style="margin-left: 18px">
						<button type="button" id="btn-add"
							class="btn btn-primary pull-left">
							<i class="fa fa-plus"></i> Tambah
						</button>
					</div>
				</div>
			</div>
		</div>

		<div class="box box-info">
			<div class="box-body">
				<table class="table" id="tableDetail">
					<thead>
						<tr>
							<th>Bahan</th>
							<th>Qty</th>
							<th>Action</th>
						</tr>
					</thead>

				</table>

				
			</div>
		</div>
	</section>
</form>

<!-- Modal -->
<div id="modal-input" class="modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-close"></i>
				</button>
				<h4>FORM PENGEMBALIAN</h4>
			</div>
			<div class="modal-body"></div>
		</div>
	</div>
</div>
<script>

function namex(id) {
	var idx = id;
	$
			.ajax({
				url : 'return/batal_retur.json',
				type : 'post',
				data : {
					idx : idx
				},

				success : function(data) {

					if (data.success) {

						$('#tableDetail').empty();
						$('#tableDetail').append(
								' ' +  ' <tr> '
								+ '   <th></th> '
								+ '   <th>Bahan</th> '
								+ '   <th>Jumlah</th> '
								+ '   <th>Action</th> '
								+ ' </tr> ');
						$
								.each(
										data.rtrList,
										function(index, jdt) {
											$('#tableDetail')
													.append(
															' '								
															+ ' <tr> '
														    + '   <td>'+ jdt.bahan.nama +'</td> '
														    + '   <td><input type="text"  id="qty_'+index+'" style="width: 30px;" /></td> '
															+ '   <td><button type="button" class="btn btn-danger btn-xs btn-delete" onclick="namex('+jdt.bahan.id+')"><i class="fa fa-trash"></i></button></td> '
														    + ' </tr> ');
										});

					}
				}
			});

}


$(document).ready(function() {
	
	
	// POP-UP Btn-add
	$("#btn-add").on("click", function() {
		$.ajax({
		url : 'return/add.html',
	    type : 'get',
		dataType : 'html',
		success : function(data) {
	    $("#modal-input").find(".modal-body").html(data);
		$("#modal-input").modal('show');
										}
									});

								});

	// button save ke list sementara
	$("#modal-input").on("submit","#form-return",function() {
						
		var rtrPilih = new Array();			
		$('input[name="rtrPilih"]:checked').each(			
			function() {rtrPilih.push(this.value);					
		});
						
						var itemPilih = JSON.stringify(rtrPilih);
						console.log(itemPilih);
						$
								.ajax({
									url : 'return/pilih_return.json',
									type : 'post',
									data : {
										itemPilih : itemPilih
									},
									success : function(data) {

										if (data.success) {
											$("#modal-input").modal('hide');
											$('#tableDetail').empty();
											$('#tableDetail').append(
															' '
													+ ' <tr> '
													+ '   <th>Bahan</th> '
													+ '   <th>Jumlah</th> '
													+ '   <th>Action</th> '
													+ ' </tr> ');
											
											
									$.each(data.rtrList,function(index,jdt) {
											$('#tableDetail').append(
																				
													' '								
													+ ' <tr> '
													+ '   <td>'+ jdt.bahan.nama +'</td> '
												    + '   <td><input type="text"  id="qty_'+index+'" style="width: 30px;" /></td> '
													+ '   <td><button type="button" class="btn btn-danger btn-xs btn-delete" onclick="namex('+jdt.bahan.id+')"><i class="fa fa-trash"></i></button></td> '
												    + ' </tr> ');
															});
										}
									}
								});
						return false;
					});
});
</script>