<form id="form-supplier" action="save" method="post" onsubmit="cekket();">
	<div class="form-horizontal">
		<input type="hidden" id="proses" name="proses" class="form-control" value="insert">
		
		<div class="form-group">
			<label class="control-label col-md-2">NAMA</label>
			<div class="col-md-6">
				<input type="text" id="nama" name="nama"  class="form-control"  required="required">
				<img id="img2" alt="" src="${pageContext.request.contextPath}/alert.png" style="display: none;">
				<a id="alt2" style="display: none;"><font color="red">Field nama harus diisi</font></a>
			</div>					
		</div>
			
		<div class="form-group">
			<label class="control-label col-md-2">TIPE</label>
			<div class="col-md-6">
				<select id="tipe" name="tipe" required="required">
					<option value="">--- Pilih Tipe ---</option>
					<option value="PT">PT</option>
					<option value="CV">CV</option>
					<option value="Individu">Individu</option>
				</select>
				<img id="img3" alt="" src="${pageContext.request.contextPath}/alert.png" style="display: none;">
				<a id="alt3" style="display: none;"><font color="red">Field tipe harus diisi</font></a>
			</div>					
		</div>
	
		<div class="form-group">
			<label class="control-label col-md-2">EMAIL</label>
			<div class="col-md-6">
				<input type="email" id="email" name="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" class="form-control" required="required">
				<img id="img3" alt="" src="${pageContext.request.contextPath}/alert.png" style="display: none;">
				<a id="alt3" style="display: none;"><font color="red">Field email harus diisi</font></a>
			</div>					
		</div>
	
		<div class="form-group">
			<label class="control-label col-md-2">TELEPHON</label>
			<div class="col-md-6">
				<input type="tel" id="telephon" name="telephon" pattern="^\d{12}$" class="form-control"  required="required">
				<img id="img3" alt="" src="${pageContext.request.contextPath}/alert.png" style="display: none;">
				<a id="alt3" style="display: none;"><font color="red">Field telepon harus diisi</font></a>
			</div>					
		</div>
	</div>
	
	<div class="modal-footer">
		<button type="submit" class="btn btn-success" onsubmit="cekket();">Simpan</button>
	</div>
</form>

<script type="text/javascript">
	
	
</script>
