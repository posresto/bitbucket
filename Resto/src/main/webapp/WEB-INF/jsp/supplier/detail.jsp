<form id="form-supplier" method="post">
	<div class="form-horizontal">
	
		<input type="hidden" id="id_sup" name="id_sup" class="form-control" value="${item.id_sup}">
		
		<div class="form-group">
			<label class="control-label col-md-2">Nama</label>
			<div class="col-md-6">
				<input type="hidden" id="nama" name="nama" class="form-control" value="${item.nama}">
				<input type="text" id="dnama" name="dnama" class="form-control" value="${item.nama}" disabled="disabled">
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Tipe</label>
			<div class="col-md-6">
				<input type="hidden" id="tipe" name="tipe" class="form-control" value="${item.tipe}">
				<input type="text" id="dtipe" name="dtipe" class="form-control" value="${item.tipe}" disabled="disabled">
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Email</label>
			<div class="col-md-6">
				<input type="hidden" id="email" name="email" class="form-control" value="${item.email}">
				<input type="text" id="demail" name="demail" class="form-control" value="${item.email}" disabled="disabled">
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Telephon</label>
			<div class="col-md-6">
				<input type="hidden" id="telephon" name="telephon" class="form-control" value="${item.telephon}">
				<input type="text" id="dtelephon" name="dtelephon" class="form-control" value="${item.telephon}" disabled="disabled">
			</div>					
		</div>
		
		
	</div>

</form>
