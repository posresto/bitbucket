<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<form id="form-supplier" method="post">
	<div class="form-horizontal">
		<input type="hidden" id="proses" name="proses" class="form-control" value="update">
		<input type="hidden" id="id_sup" name="id_sup" class="form-control" value="${item.id_sup}">
		
		<div class="form-group">
			<label class="control-label col-md-2">Nama</label>
			<div class="col-md-6">
				<input type="text" id="nama" name="nama" class="form-control" value="${item.nama}">
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Tipe</label>
			<div class="col-md-6">
				<select id="tipe" name="tipe">
					<option value="PT"
						<c:if test="${item.tipe == 'PT'}">
							<c:out value="selected"/>
						</c:if>>PT
					</option>
					
					<option value="CV"
						<c:if test="${item.tipe == 'CV'}">
							<c:out value="selected"/>
						</c:if>>CV
					</option>
					
					<option value="Individu"
						<c:if test="${item.tipe == 'Individu'}">
							<c:out value="selected"/>
						</c:if>>Individu
					</option>
					
				</select>
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Email</label>
			<div class="col-md-6">
				<input type="email" id="email" name="email" class="form-control" value="${item.email}">
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Telephon</label>
			<div class="col-md-6">
				<input type="text" id="telephon" name="telephon" class="form-control" value="${item.telephon}">
			</div>					
		</div>
		
	</div>

	<div class="modal-footer">
		<button type="submit" class="btn btn-success">Simpan</button>
	</div>
</form>
