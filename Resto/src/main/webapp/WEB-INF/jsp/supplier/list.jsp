<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<c:forEach var="item" items="${list}">
	<tr>
		<td>${item.nama}</td>
		<td>${item.telephon}</td>
		<td>
			<button type="button" class="btn btn-success btn-xs btn-edit" value="${item.id_sup}"><i class="fa fa-edit"></i></button>
			<button type="button" class="btn btn-danger btn-xs btn-delete" value="${item.id_sup}"><i class="fa fa-trash"></i></button>
			<button type="button" class="btn btn-info btn-xs btn-detail" value="${item.id_sup}"><i class="fa fa-eye"></i></button>
		</td>
	</tr>
</c:forEach>